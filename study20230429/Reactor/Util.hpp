#pragma once 

#include <iostream>
#include <string>
#include <unistd.h>
#include <fcntl.h>

class Util
{
public:
    static void SetNonBlock(int fd)
    {
        int fl = fcntl(fd,F_GETFL);
        fcntl(fd,F_SETFL,fl | O_NONBLOCK);
    }
};