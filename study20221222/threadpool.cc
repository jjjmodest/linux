#include "threadpool.hpp"

const std::string opera = "+-*/%";

int main()
{
    unique_ptr<Threadpool<int>> utp(new Threadpool<int>());
    utp->start();

    srand((unsigned long)time(nullptr) ^ getpid());
    while (1)
    {
        int in = rand() % 1231;
        cout<<in<<endl;
        utp->push(in);
        sleep(1);
    }
}