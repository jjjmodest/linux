public:
    static Threadpool<T> *getinstance()
    {
        static pthread_mutex_t mutex;
        if (instance == nullptr)
        {
            pthread_mutex_lock(&mutex);
            if (instance == nullptr)
            {
                instance = new Threadpool<T>();
            }
            pthread_mutex_unlock(&mutex);
        }
        assert(instance != nullptr);
        return instance;
    }

private:
    bool _isStart;
    uint32_t _threadnum;
    queue<T> _taskqueue;
    // 让线程互斥的获取任务队列的任务
    pthread_mutex_t _mutex;
    pthread_cond_t _cond;

    static Threadpool<T> *instance;
};

template <class T>
Threadpool<T> *Threadpool<T>::instance = nullptr;