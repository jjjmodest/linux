#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include "lock.hpp"
using namespace std;


int count = 0;

int num = 10000;

Mutex mutex;

void *fuc()
{
    Mutex_smart_ptr msp(&mutex);
    cout<<"I'm "<<pthread_self()<<"NoW, CounT is "<<count<<endl;
    count++;
}

void * func(void *argc)
{
    char * msg = static_cast<char *>(argc);
    for (int i = 0; i < num; i++)
    {
        fuc();
    }
}

int main()
{
    pthread_t tid;
    pthread_t tid1;

    pthread_create(&tid, NULL, func, (void *)"我是线程 1 ");
    pthread_create(&tid1, NULL, func, (void *)"我是线程 2 ");

    pthread_join(tid, NULL);
    pthread_join(tid1, NULL);

    cout << "The count is : " << count << endl;
    return 0;
}
// int count = 0;

// int num = 88888;

// pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

// typedef struct ThreadDate
// {
//     string threadname;
//     pthread_mutex_t *mutex_p;
// } Td;

// void *fuc(void *argc)
// {
//     Td *td = static_cast<Td *>(argc);
//     for (int i = 0; i < num; i++)
//     {
//         pthread_mutex_lock(td->mutex_p);
//         count++;
//         cout << "我是" << td->threadname << "我的线程ID是: " << pthread_self() << "当前count为 : " << count << endl;
//         pthread_mutex_unlock(td->mutex_p);
//     }
// }

// int main()
// {
//     // pthread_mutex_t mutex;
//     // pthread_mutex_init(&mutex, NULL);

//     Td *td1 = new Td();
//     td1->threadname = "我是thread 1";
//     td1->mutex_p = &mutex;

//     Td *td2 = new Td();
//     td2->threadname = "我是thread 2";
//     td2->mutex_p = &mutex;

//     pthread_t tid;
//     pthread_t tid1;

//     pthread_create(&tid, NULL, fuc, (void *)td1);
//     pthread_create(&tid1, NULL, fuc, (void *)td2);

//     pthread_join(tid, NULL);
//     pthread_join(tid1, NULL);

//     pthread_mutex_destroy(&mutex);
//     cout << "The count is : " << count << endl;
//     return 0;
// }
