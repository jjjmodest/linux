#include <iostream>
#include <pthread.h>
#include <unistd.h>
using namespace std;

class Mutex
{
public:
    Mutex()
    {
        pthread_mutex_init(&_mutex, nullptr);
    }
    void lock()
    {
        pthread_mutex_lock(&_mutex);
    }
    void unlock()
    {
        pthread_mutex_unlock(&_mutex);
    }
    ~Mutex()
    {
        pthread_mutex_destroy(&_mutex);
    }

private:
    pthread_mutex_t _mutex;
};

class Mutex_smart_ptr
{
public:
    Mutex_smart_ptr(Mutex *mutex) 
        :_mutex(mutex)
    {                                                                           
        _mutex->lock();
    }
    ~Mutex_smart_ptr()
    {
        _mutex->unlock();
    }
private:
    Mutex *_mutex;
};