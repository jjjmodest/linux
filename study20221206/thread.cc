#include <iostream>
#include <pthread.h>
#include <unistd.h>
using namespace std;

//条件变量
pthread_cond_t cond;
//mutex
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
//线程运行的函数
void *fuc(void *argc)
{
    while (1)
    {
        pthread_cond_wait(&cond, &mutex);
        cout << "my thread id : " << pthread_self() << endl;
    }
}

int main()
{
    pthread_t t1, t2, t3;

    pthread_create(&t1, nullptr, fuc, NULL);
    pthread_create(&t2, nullptr, fuc, NULL);
    pthread_create(&t3, nullptr, fuc, NULL);

    while (1)
    {
        char a;
        cout << "请输入n/q : ";
        cin >> a;
        //n为next q为quit
        if (a == 'n')
        {
            //通过条件变量唤醒一个线程
            pthread_cond_signal(&cond);
        }
        else
        {
            break;
        }

        sleep(0.5);
    }

    pthread_cancel(t1);
    pthread_cancel(t2);
    pthread_cancel(t3);

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    pthread_join(t3, NULL);
    pthread_cond_destroy(&cond);
    return 0;
}
