#include <stdio.h> 
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>


int main()
{
  int fd = open("test.txt",O_RDWR);
  
  if(fd < 0)
  {
    perror("open");
    return 1;
  }
  dup2(fd,0);
  //dup2(fd,1);
  //fprintf(stdout,"打开成功，我的文件描述符是%d\n",fd);
  //fflush(stdout);
  
  char a[128];
  while(fgets(a,sizeof(a),stdin) != NULL)
  {
    printf("%s",a);
  }

  fflush(stdout);

  close(fd);

  return 0;
}
