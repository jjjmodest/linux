#pragma once

#include <iostream>
#include <vector>
#include <string>
using namespace std;

#define SEP 'X'
#define SEP_SZ sizeof(SEP)

void PackageSplit(string &buff, vector<string> *result)
{
    while (true)
    {
        size_t pos = buff.find(SEP);
        if (pos == string::npos)
            break;
        result->push_back(move(buff.substr(0, pos)));
        buff.erase(0, pos + SEP_SZ);
    }
}