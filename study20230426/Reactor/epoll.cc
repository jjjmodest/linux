#include "Tcpserver.hpp"
#include "Sock.hpp"
#include <memory>

using namespace std;

void Usage(string process)
{
    cout << "Please entry" << process << " port" << endl;
}

int HandlerPro(Connection *conn, string &message)
{
    // 我们能保证走到这里一定是完整的报文，已经解码
    // 接下来是反序列化
    cout << "获取request : " << message << endl;
}

int main(int argv, char **argc)
{
    if (argv != 2)
    {
        Usage(argc[0]);
        exit(1);
    }
    unique_ptr<Tcpserver> ep(new Tcpserver(HandlerPro, atoi(argc[1])));
    ep->Run();
    return 0;
}