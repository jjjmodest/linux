#pragma once

#include <iostream>
#include <string>
#include <cstdlib>
#include <sys/epoll.h>
#include <cassert>
#include <functional>
#include "Sock.hpp"
#include "Log.hpp"
using namespace std;

class EpollServer
{
public:
    static int const gsize = 128;
    static int const num = 256;
    using fucs_t = function<void(int)>;

    EpollServer(uint16_t port, fucs_t fuc)
        : port_(port), listensock_(-1), epfd_(-1), fuc_(fuc)
    {
    }
    void InitEpollServer()
    {
        listensock_ = Sock::Socket();
        Sock::Bind(listensock_, port_);
        Sock::Listen(listensock_);

        epfd_ = epoll_create(gsize);
        if (epfd_ < 0)
        {
            logMessage(FATAL, "%d:%s", errno, strerror(errno));
            exit(1);
        }
        logMessage(DEBUG, "epoll_creatr success,epoll模型创建成功,epfd: %d", epfd_);
    }
    void HandlerEvent(struct epoll_event *revs, int n)
    {
        for (int i = 0; i < n; i++)
        {
            int sock = revs[i].data.fd;
            uint32_t revent = revs[i].events;
            if (revent & EPOLLIN)
            {
                // IO
                if (sock == listensock_)
                {
                    // 监听套接字
                    string clientip;
                    uint16_t clientport = 0;
                    int iosock = Sock::Accept(listensock_, &clientip, &clientport);
                    if (iosock < 0)
                    {
                        logMessage(FATAL, "Sock error , errno : %d :%s", errno, strerror(errno));
                        continue;
                    }
                    // 托管给epoll
                    struct epoll_event ev;
                    ev.data.fd = iosock;
                    ev.events = EPOLLIN;
                    int a = epoll_ctl(epfd_, EPOLL_CTL_ADD, iosock, &ev);
                    assert(a == 0);
                    (void)a;
                }
                else
                {
                    // 其他套接字
                    fuc_(sock);
                }
            }
            else
            {
            }
        }
    }
    void RunServer()
    {
        // 1.先添加listensock_到epoll模型中
        struct epoll_event ev;
        ev.events = EPOLLIN;
        ev.data.fd = listensock_;
        int a = epoll_ctl(epfd_, EPOLL_CTL_ADD, listensock_, &ev);
        if (a != 0)
        {
            logMessage(FATAL, "%d:%s", errno, strerror(errno));
            exit(1);
        }
        struct epoll_event revs[num];
        int timeout = 10000;
        while (1)
        {
            int n = epoll_wait(epfd_, revs, num, timeout);
            switch (n)
            {
            case 0:
                // timeout
                cout << "timeout ... : " << (unsigned int)time(nullptr) << endl;
                break;
                // error
                cout << "epoll_wait error : " << strerror(errno) << endl;
            case -1:
                break;
            default:
                // 等待成功
                HandlerEvent(revs, n);
                break;
            }
        }
    }

    ~EpollServer()
    {
        if (listensock_ != -1)
            close(listensock_);
        if (epfd_ != -1)
            close(epfd_);
    }

private:
    int listensock_;
    int epfd_;
    uint16_t port_;
    fucs_t fuc_;
};