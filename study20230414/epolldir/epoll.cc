#include "EpollServer.hpp"
#include "Sock.hpp"
#include <memory>

void Usage(string process)
{
    cout << "Please entry" << process << " port" << endl;
}

void myfuc(int sock)
{
    // 这里bug，TCP基于流式，如何保证一次将数据读取完毕，一会解决
    char buff[1024];
    int sz = recv(sock, buff, sizeof buff -1, 0);
    if (sz > 0)
    {
        buff[sz] = 0;
        logMessage(DEBUG, "client[%d]:%s", sock, buff);
    }
    else if (sz == 0)
    {
        cout << "client[" << sock << "] quit" << endl;
        close(sock);
    }
    else
    {
        cout << "recv error" << endl;
        close(sock);
    }
}

int main(int argv, char **argc)
{
    if (argv != 2)
    {
        Usage(argc[0]);
        exit(1);
    }
    unique_ptr<EpollServer> epoll(new EpollServer(atoi(argc[1]), myfuc));
    epoll->InitEpollServer();
    epoll->RunServer();
    return 0;
}