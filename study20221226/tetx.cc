bool Socket()
{
    fd_ = socket(AF_INET, SOCK_DGRAM, 0);
    if (fd_ < 0)
    {
        perror("socket");
        return false;
    }
    return true;
}
bool Close()
{
    close(fd_);
    return true;
}
bool Bind(const std::string &ip, uint16_t port)
{
    sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(ip.c_str());
    addr.sin_port = htons(port);
    int ret = bind(fd_, (sockaddr *)&addr, sizeof(addr));
    if (ret < 0)
    {
        perror("bind");
        return false;
    }
    return true;
}