#include <iostream>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <string>
#include "log.hpp"
using namespace std;

class UdpServer
{
public:
    UdpServer()
    {
    }
    ~UdpServer()
    {
    }

public:
    void init()
    {
    }
    void start()
    {
    }

private:
    int socketfd;
};
int main()
{
    int fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (fd < 0)
    {
        logMessage(FATAL, "%s%d", strerror(errno),fd);
        exit;
    }
    logMessage(DEBUG, "socket create success : %d",fd);

    return 0;
}