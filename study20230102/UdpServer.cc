#include <iostream>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <string>
#include <unordered_map>
#include <cstring>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "log.hpp"
using namespace std;

static void Usage(const string proc)
{
    cout << "Usage:\n\t" << proc << " port [ip]" << endl;
}

class UdpServer
{
public:
    UdpServer(int port, string ip = "") : _ip(ip), _sockfd(-1), _port((uint16_t)port)
    {
    }
    ~UdpServer()
    {
    }

public:
    void init()
    {
        // 1.create 套接字
        _sockfd = socket(AF_INET, SOCK_DGRAM, 0);
        if (_sockfd < 0)
        {
            logMessage(FATAL, "socket: %s%d", strerror(errno), _sockfd);
            exit(1);
        }
        logMessage(DEBUG, "socket create success : %d", _sockfd);
        // 2 bind
        // 2.1 填入基本信息到struct sockaddr_in 中
        struct sockaddr_in local;
        // 初始化
        bzero(&local, sizeof(local));
        // 填充域 AF_INET 网络通信 AF_UNIX 本地通信
        local.sin_family = AF_INET;
        // 填充对应的端口号 htons的作用是将本地序列转换为网络序列这样才能发送给对方
        local.sin_port = htons(_port);
        // 服务器的IP地址 xx.yy.aa.ccc 每个都是0-255的数字，有四个8比特位 正好放在uint36_t中
        // sin_addr也是一个结构体其中的元素是s._addr是被typedef过的uint32_t
        // INADDR_ANY就是0，一般不关注服务器绑定哪一个IP地址，服务器会自动bind，一般所有服务器都是这样做的
        // inet_addr将char *转换为s_addr，还会将主机序列转换为网络序列
        local.sin_addr.s_addr = _ip.empty() ? INADDR_ANY : inet_addr(_ip.c_str());
        // 2.2 bind网络信息
        if (bind(_sockfd, (const struct sockaddr *)&local, sizeof(local)) == -1)
        {
            logMessage(FATAL, "bind: %s%d", strerror(errno), _sockfd);
            exit(2);
        }
        logMessage(DEBUG, "socket success: %d", _sockfd);
    }
    void start()
    {
        char inbuffer[1024];  // 输入进来的数据放到inbuffer中
        char outbuffer[1024]; // 输出的数据放outbuffer中
        int i = 1;
        while (1)
        {
            struct sockaddr_in peer;      // 输出形参数
            socklen_t len = sizeof(peer); // 输入输出型参数
            ssize_t size = recvfrom(_sockfd, inbuffer, sizeof(inbuffer) - 1, 0, (struct sockaddr *)&peer, &len);
            if (size > 0)
            {
                // 这里将读的数据看为字符串
                inbuffer[size] = 0;
            }
            else if (size == -1)
            {
                logMessage(WARNING, "recevfrom : %s %d", strerror(errno), _sockfd);
            }
            // 拿到发送方的IP地址 peer.sin_addr的类型是四字节uint36_t 要转换为string
            // peer.sin_port是从网络中接收到的是网络序列，ntohs目的是将网络序列转换为本地序列
            string peerip = inet_ntoa(peer.sin_addr);
            uint16_t peerport = ntohs(peer.sin_port);
            // 打印客户端IP与port 和信息
            logMessage(NOTICE, "[%s %d]# %s", peerip.c_str(), peerport, inbuffer);
            // 检查是否存在该用户
            checkOnlineUser(i, peerip, peerport, peer);
            // 消息路由
            messageRoute(inbuffer);
            // // 转换字符串小写-大写
            // for (int i = 0; i < strlen(inbuffer); i++)
            // {
            //     if (isalpha(inbuffer[i]) && islower(inbuffer[i]))
            //     {
            //         outbuffer[i] = toupper(inbuffer[i]);
            //     }
            //     else
            //     {
            //         outbuffer[i] = inbuffer[i];
            //     }
            // }
            // sendto(_sockfd, outbuffer, strlen(outbuffer), 0, (struct sockaddr *)&peer, len);
            // memset(outbuffer, 0, sizeof(outbuffer));
        }
    }
    void messageRoute(string message)
    {
        for (auto &ch : user)
        {
            sendto(_sockfd, message.c_str(), message.size(), 0, (struct sockaddr *)&(ch.second),sizeof(ch.second));
        }
    }

    bool checkOnlineUser(int &i, string &ip, uint16_t port, struct sockaddr_in &peer)
    {
        string userInfor = ip;
        userInfor += " ";
        userInfor += to_string(port);

        auto iter = user.find(userInfor);
        if (iter == user.end())
        {
            // 没找到
            user.insert({userInfor, peer});
            i = 1;
            if (i == 1)
            {
                cout << "新用户登录" << endl;
            }
        }
        else
        {
            // 找到了
            if (i == 1)
            {
                i = 0;
                cout << "老用户登录" << endl;
            }
        }
    }

private:
    string _ip;
    int _sockfd;
    uint16_t _port;
    unordered_map<string, struct sockaddr_in> user;
};
int main(int argc, char *argv[])
{
    if (argc != 2 && argc != 3)
    {
        Usage(argv[0]);
        exit(3);
    }
    uint16_t port = atoi(argv[1]);
    string ip;
    if (argc == 3)
    {
        ip = argv[2];
    }

    UdpServer svr(port, ip);
    svr.init();
    svr.start();

    return 0;
}