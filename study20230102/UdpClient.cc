#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <cassert>
#include <cstring>
#include <strings.h>
#include "log.hpp"
using namespace std;

struct sockaddr_in server;

static void Usage(const string proc)
{
    cout << "Usage:\n\t"
         << "server IP ,server port" << endl;
}

void *fuc(void *argc)
{
    while (1)
    {
        int sockfd = *(int *)argc;
        // 接收
        char buffer[1024];
        memset(buffer, 0, sizeof(buffer));
        struct sockaddr temp;
        socklen_t len = sizeof(temp);
        ssize_t s = recvfrom(sockfd, buffer, sizeof(buffer), 0, (struct sockaddr *)&server, &len);
        if (s > 0)
        {
            buffer[s] = 0;
            cout << "Server output| " << buffer << endl;
        }
    }
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        Usage(argv[0]);
        exit(1);
    }
    // 1. 获取服务端
    string serverip = argv[1];
    uint16_t serverport = atoi(argv[2]);

    // 2. 创建客户端
    // 2.1 创建socket
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    assert(sockfd > 0);

    // 2.2 client 需不需要bind 可以不用bind OS会自动帮我们bind 不推荐自己bind
    // 2.3 填写对应信息

    bzero(&server, sizeof(server));
    // 都需要转换为网络序列
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(serverip.c_str());
    server.sin_port = htons(serverport);

    pthread_t t;
    pthread_create(&t, nullptr, fuc, (void *)&sockfd);

    // 3. 发送消息
    string output;
    while (1)
    {

        cerr << "Please entry | ";
        getline(cin, output);

        // 发送
        sendto(sockfd, output.c_str(), output.size(), 0, (const struct sockaddr *)&server, sizeof(server));
    }
    close(sockfd);
    return 0;
}