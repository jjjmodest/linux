#include "util.hpp"

#define CRLF "\r\n"
#define SPACE " "
#define SPACE_LEN strlen(SPACE)
#define HOME_PAGE "index.html"
#define WEB_ROOT "./wwwroot"

string getPath(string http_request)
{
    ssize_t pos = http_request.find(CRLF);
    if (pos == string::npos)
        return "";
    string request_line = http_request.substr(0, pos);
    // GET /a/b/c http/1.1

    ssize_t firstSpace = request_line.find(SPACE);
    if (firstSpace == string::npos)
        return "";

    ssize_t secondSpace = request_line.rfind(SPACE);
    if (secondSpace == string::npos)
        return "";

    string path = request_line.substr(firstSpace + SPACE_LEN, secondSpace - (firstSpace + SPACE_LEN));
    
    if (path.size() == 1 && path[0] == '/')
        path += HOME_PAGE; // 如果只发了/ ，我们就将我们网址的主页返回过去
    return path;
}

string readFile(const string &path)
{
    ifstream in(path, std::ifstream::binary);
    if (!in.is_open())
        return "404";
    string content;
    string line;
    while (getline(in, line))
        content += line;
    in.close();
    return content;
}

void handlerHttpRequest(int sock)
{
    char buffer[10240];
    ssize_t sz = read(sock, buffer, sizeof(buffer));
    if (sz > 0)
    {
        cout << buffer;
    }

    string path = getPath(buffer);
    cout << "path->" << path << endl;

    string resource = WEB_ROOT;
    resource += path;
    cout << "resource->" << resource << endl;

    string html = readFile(resource);

    string response;
    response += "HTTP/1.0 200 OK\r\n"; // response请求行
    response += "Content-Type: text/html\r\n";
    response += ("Content-Length: " + to_string(html.size()) + "\r\n");
    response += "\r\n"; // 本行为\r\n为请求报头与正文的分界线 请求报头暂且为空
    response += html;   // 正文

    send(sock, response.c_str(), response.size(), 0);
}


using namespace std;

void handlerHttpRequest1(int sock)
{
    char buffer[10240];
    ssize_t s = read(sock, buffer, sizeof buffer);
    if(s > 0) cout << buffer;

    std::string path = getPath(buffer);

    std::string recource = WEB_ROOT;
    recource += path;
    std::cout << recource << std::endl;

    std::string html = readFile(recource);

    //开始响应
    std::string response;
    response = "HTTP/1.0 200 OK\r\n";
    response += "Content-Type: text/html\r\n";
    response += ("Content-Length: " + std::to_string(html.size()) + "\r\n");
    response += "\r\n";
    response += html;

    send(sock, response.c_str(), response.size(), 0);
}


class Tcpserver
{
public:
    Tcpserver(uint16_t port, const string &ip = "")
        : _sock(-1), _port(port), _ip(ip)
    {
        _quit = false;
    }
    ~Tcpserver()
    {
        if (_sock >= 0)
            close(_sock);
    }

public:
    void init()
    {
        _sock = socket(AF_INET, SOCK_STREAM, 0);
        if (_sock < 0)
        {
            exit(1);
        }
        struct sockaddr_in local;
        memset(&local, 0, sizeof(local));
        local.sin_family = AF_INET;
        local.sin_port = htons(_port);
        _ip.empty() ? INADDR_ANY : (inet_aton(_ip.c_str(), &local.sin_addr));
        if (bind(_sock, (const sockaddr *)&local, sizeof(local)) < 0)
        {
            exit(2);
        }
        if (listen(_sock, 5) < 0)
        {
            exit(3);
        }
    }
    void start()
    {
        signal(SIGCHLD, SIG_IGN);
        while (!_quit)
        {
            struct sockaddr_in peer;
            socklen_t len = sizeof(peer);
            int servicesock = accept(_sock, (struct sockaddr *)&peer, &len);
            if (_quit)
                break;
            if (servicesock < 0)
            {
                cerr << "accept error ..." << endl;
                continue;
            }

            int clientport = ntohs(peer.sin_port);
            string clientip = inet_ntoa(peer.sin_addr);

            pid_t pid = fork();
            assert(pid != -1);
            if (pid == 0)
            {
                if (fork() > 0)
                    exit(4);
                handlerHttpRequest(servicesock);
                exit(0);
            }
            close(servicesock);
            wait(nullptr);
        }
    }
    void safequit()
    {
        _quit = true;
    }

private:
    int _sock;
    uint16_t _port;
    string _ip;
    // 安全退出
    bool _quit;
};

Tcpserver *svrp = nullptr;
void sighandler(int sig)
{
    if (sig == 3 && svrp != nullptr)
        svrp->safequit();
    cout << "server quit" << endl;
}
