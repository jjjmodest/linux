#include <iostream>
#include <unistd.h>
#include <signal.h>
#include <ctime>
#include <cstdlib>
using namespace std;


void fuc(int sig)
{
    //如果进行到这一步说明信号抵达成功，证明阻塞该信号失败，代码出问题了
    cout << "I caught a signal,it is : " << sig << endl;
}

static void ShowPending(sigset_t *pending)
{
    for (int sig = 1; sig <= 31; sig++)
    {
        if (sigismember(pending, sig))
        {
            cout << "1";
        }
        else
        {
            cout << "0";
        }
    }
    cout << endl;
}

int main()
{
    sigset_t pending;

    //将所有的信号屏蔽
    sigset_t set, oldset;
    sigemptyset(&set);
    sigemptyset(&oldset);
    sigfillset(&set);
    sigprocmask(SIG_SETMASK, &set, &oldset);

    while (1)
    {
        sigemptyset(&pending);
        if (sigpending(&pending) == 0)
        {
            //打印当前的 pending表
            ShowPending(&pending);
        }

        //使用随机数来发送信号
        srand((unsigned)time(NULL));
        int sig = rand() % 31;
        if (sig == 6 || sig == 9 || sig == 19)
        {
            //发送6和9信号，尽管屏蔽了，但是还是会结束进程
            // 19信号会让当前进程变为后台进程
            ;
        }
        else if (sig == 0)
        {
            //没有0号信号
            ;
        }
        else
        {
            //向自己发送信号
            raise(sig);
        }
        sleep(1);
    }
    return 0;
}

// int main()
// {
//     while(1)
//     {
//         ;
//     }
// }

// int main()
// {
//     sigset_t set, oldset;
//     sigemptyset(&set);
//     sigaddset(&set, SIGINT);

//     // block SIGNINT and save previous block in oldset
//     sigprocmask(SIG_BLOCK, &set, &oldset);

//     //尽管设置捕捉信号和自定义处理动作，但是该信号已被阻塞
//     signal(SIGINT, fuc);

//     int cnt = 5;
//     while (cnt--)
//     {
//         cout << ".........." << endl;
//     }

//     //向自己发送信号
//     raise(2);

//     cout<<"发送成功"<<endl;

//     //解除阻塞
//     sigprocmask(SIG_SETMASK,&oldset,NULL);

//     return 0;
// }