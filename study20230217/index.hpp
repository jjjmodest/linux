#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <fstream>
#include "util.hpp"
using namespace std;

namespace ns_indx
{
    typedef struct DocInfo
    {
        string title;    // 文档的标题
        string content;  // 文档去标签化之后的内容
        string url;      // 该文档在官网中的url
        uint64_t doc_id; // 文档的id
    } DocInfo_t;

    struct InvertedElem
    {
        // 倒排索引
        uint64_t doc_id; // 文档的id
        string word;     // 关键词
        int weight;      // 权重
    };
    // 倒排拉链
    typedef vector<InvertedElem> InvertedList; // 该数组存储着该关键词对应的一系列InvertedElem

    class Index
    {
    private:
        // 正排索引的数据结构用数组，数组下标就是文档id
        vector<DocInfo_t> forward_index; // 正排索引
        // 倒排索引一定是一个词对应一个InvertedElem
        unordered_map<string, InvertedList> inverted_index; // 倒排索引
    public:
        // 根据doc_id找到文档内容
        DocInfo_t *GetforwardIndex(uint64_t doc_id)
        {
            if (doc_id >= forward_index.size())
            {
                cerr << "doc_id beyond range" << endl;
                return nullptr;
            }
            return &forward_index[doc_id];
        }
        // 根据string找到倒排拉链
        InvertedList *GetInvertedList(const string &word)
        {
            auto iter = inverted_index.find(word);
            if (iter == inverted_index.end())
            {
                cerr << "word is not exit" << endl;
                return nullptr;
            }
            return &(iter->second);
        }

        // 根据去标签后的内容格式化的文档，建立倒排和正排索引
        // data/raw_html/raw.txt
        bool BuildIndex(const string &input)
        {
            ifstream in(input, ios::in | ios::binary);
            if (!in.is_open())
            {
                cerr << "Sorry this " << input << "open failed." << endl;
                return false;
            }

            string line;
            while (getline(in, line))
            {
                DocInfo_t *doc = BulidForwardIndex(line);
                if (doc == nullptr)
                {
                    cerr << "build " << line << "error" << endl; // for debug
                    continue;
                }

                BulidInvertedIndex(*doc);
            }

            return true;
        }

    private:
        DocInfo_t *BulidForwardIndex(const string &line)
        {
            // 1. 解析line，将字符串分割
            vector<string> results;
            const string sep = "\3"; // 行内分隔符
            ns_util::IndexUtil::CutString(line,&results,sep);
            if(results.size() != 3)
            {
                return nullptr; 
            }
            // 2. 将分割完的字符串填充到DocInfo_t中
            DocInfo_t doc;
            doc.title = results[0];
            doc.content = results[1];
            doc.url = results[2];
            doc.doc_id = forward_index.size(); // 先保存doc_id，再插入，对应的id就是当前doc在vector中的下标!
            // 3. 插入到正排索引的vector forward_index中 
            forward_index.push_back(doc);
            return &forward_index.back(); // 将最后一个元素返回 
        }
        bool BulidInvertedIndex(const DocInfo_t &doc)
        {
            return true; 
        }
    };
}