#include <iostream>
#include <unistd.h>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <sys/types.h>
#include <sys/wait.h>
#include <vector>
#include <cassert>
using namespace std;

typedef void (*fuc_pipe)();

vector<fuc_pipe> vfus;

void f1()
{
    cout << "自毁程序已启动,执行的进程为:[" << getpid() << "]"
         << "执行时间为:" << time(nullptr) << endl;
}
void f2()
{
    cout << "清理人类程序已启动,执行的进程为:[" << getpid() << "]"
         << "执行时间为:" << time(nullptr) << endl;
}
void f3()
{
    cout << "屠杀模式已启动,执行的进程为:[" << getpid() << "]"
         << "执行时间为:" << time(nullptr) << endl;
}

void loadfuc()
{
    vfus.push_back(f1);
    vfus.push_back(f2);
    vfus.push_back(f3);
}

int main()
{
    // 0.load fuc
    loadfuc();

    // 1.make pipe
    int pipefd[2];
    if (pipe(pipefd) != 0)
    {
        cout << "pipe error" << endl;
        exit(1);
    }

    pid_t pid = fork();
    if (pid < 0)
    {
        cout << "fork error" << endl;
        exit(2);
    }
    else if (pid == 0)
    {
        // child read
        close(pipefd[1]);
        while (true)
        {
            uint32_t opsb = 0;
            ssize_t s = read(pipefd[0], &opsb, sizeof(uint32_t));
            assert(s == sizeof(uint32_t));
            (void)s;

            if (opsb < vfus.size())
            {
                vfus[opsb]();
            }
            else
            {
                cout << "opsb error" << endl;
            }
        }
        close(pipefd[0]);
        exit(3);
    }
    else
    {
        // parent
        close(pipefd[0]);

        close(pipefd[1]);
    }
}