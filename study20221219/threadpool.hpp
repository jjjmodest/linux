#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <queue>
#include <assert.h>
#include <cstdlib>
#include <memory>

using namespace std;

int gthreadnum = 8;
template <class T>
class Threadpool
{
public:
    Threadpool(int num = gthreadnum) : _isStart(false), _threadnum(num)
    {
        assert(_threadnum > 0);
        pthread_mutex_init(&_mutex, nullptr);
        pthread_cond_init(&_cond, nullptr);
    }
    ~Threadpool()
    {
        pthread_mutex_destroy(&_mutex);
        pthread_cond_destroy(&_cond);
    }
    static void *fuc(void *argc)
    {
        Threadpool<T> *p = static_cast<Threadpool<T> *>(argc);
        while (1)
        {
            sleep(1);
            cout << pthread_self() << endl;
        }
    }
    void start()
    {
        assert(!_isStart);
        for (int i = 0; i < _threadnum; i++)
        {
            pthread_t th1;
            pthread_create(&th1, nullptr, fuc, this);
        }
        _isStart = true;
    }
    void push(const T &in)
    {
    }

private:
    T pop()
    {
    }

private:
    bool _isStart;
    uint32_t _threadnum;
    queue<T> _taskqueue;
    // 让线程互斥的获取任务队列的任务
    pthread_mutex_t _mutex;
    pthread_cond_t _cond;
};