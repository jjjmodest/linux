#pragma once

#include <iostream>
#include <string>
#include <cstdlib>
#include <cstring>
#include <sys/epoll.h>
#include "Log.hpp"
using namespace std;

class Epoller
{
public:
    static int CreateEpoll()
    {
        int size = 128;
        int epfd = epoll_create(size);
        if (epfd < 0)
        {
            logMessage(FATAL, "%d:%s", errno, strerror(errno));
            exit(1);
        }
        return epfd;
    }
    static bool Addevent(int epfd, int sock, uint32_t event)
    {
        struct epoll_event ev;
        ev.data.fd = sock;
        ev.events = event;
        int n = epoll_ctl(epfd, EPOLL_CTL_ADD, sock, &ev);
        if (n != 0)
        {
            logMessage(FATAL, "%d:%s", errno, strerror(errno));
            return false;
        }
        return true;
    }

    static bool ModEvent(int epfd, int sock, uint32_t event)
    {
        struct epoll_event ev;
        ev.data.fd = sock;
        ev.events = event;
        int n = epoll_ctl(epfd, EPOLL_CTL_MOD, sock, &ev);
        return n == 0;
    }
    
    static int GetReadyFd(int epfd, struct epoll_event evs[], int num)
    {
        // 阻塞式
        int n = epoll_wait(epfd, evs, num, -1);
        if (n == -1)
        {
            logMessage(FATAL, "%d:%s", errno, strerror(errno));
        }
        return n;
    }
};
