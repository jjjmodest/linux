#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
using namespace std;

namespace ns_indx
{
    typedef struct DocInfo
    {
        string title;    // 文档的标题
        string content;  // 文档去标签化之后的内容
        string url;      // 该文档在官网中的url
        uint64_t doc_id; // 文档的id
    } DocInfo_t;

    struct InvertedElem
    {
        // 倒排索引
        uint64_t doc_id; // 文档的id
        string word;     // 关键词
        int weight;      // 权重
    };
    // 倒排拉链
    typedef vector<InvertedElem> InvertedList; // 该数组存储着该关键词对应的一系列InvertedElem

    class Index
    {
    private:
        // 正排索引的数据结构用数组，数组下标就是文档id
        vector<DocInfo_t> forward_index; // 正排索引
        // 倒排索引一定是一个词对应一个InvertedElem
        unordered_map<string, InvertedList> inverted_index; // 倒排索引
    public:
        // 根据doc_id找到文档内容
        DocInfo_t *GetforwardIndex(uint64_t doc_id)
        {
            return nullptr;
        }
        // 根据string找到倒排拉链
        InvertedList* GetInvertedList(const string &word)
        {
            return nullptr;
        }

        // 根据去标签后的内容格式化的文档，建立倒排和正排索引
        // data/raw_html/raw.txt
        bool BuildIndex(const string& input)
        {
            return true;
        }
    
    };
}