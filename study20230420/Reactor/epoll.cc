#include "Tcpserver.hpp"
#include "Sock.hpp"
#include <memory>

using namespace std;


void Usage(string process)
{
    cout << "Please entry" << process << " port" << endl;
}



int main(int argv, char **argc)
{
    if (argv != 2)
    {
        Usage(argc[0]);
        exit(1);
    }
    unique_ptr<Tcpserver> ep(new Tcpserver(atoi(argc[1])));
    ep->Run();
    return 0;
}