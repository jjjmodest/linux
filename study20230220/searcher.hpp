#pragma once
#include "index.hpp"

namespace ns_searcher
{
    class Searchear
    {
    private:
        ns_index::Index *index; // 供系统进行查找的索引
    public:
        Searchear() {}
        ~Searchear() {}

    public:
        void InitSercher(const string &input)
        {
            // 初始化索引
            index = ns_index::Index::GetInstance();
            index->BuildIndex(input);
        }

        // query 搜索关键字
        // json_string 返回给用户浏览器的数据搜索结果
        void InitSearcher(const string &query, string *json_string)
        {
            // 1.[分词]:对我们的querry进行按照searcher的要求进行分词
            // 2.[触发]:就是根据分词的各个"词"，进行index查找
            // 3.[合并排序]:根据weight相关性进行降序排序
            // 4.[构建]:根据查找出来的结果,构建json串--jsoncpp
        }
    };
}
