#include <iostream>
#include <vector>
#include <semaphore.h>
using namespace std;

const int size = 8;

template <class T>
class Ringqueue
{
public:
    Ringqueue(int cap = size)
        : _ringqueue(cap)
    {
        // 初始化信号量
        sem_init(&_dataspace,0,_ringqueue.size());
        sem_init(&_datanum,0,0);
    }
    ~Ringqueue()
    {
        // 销毁信号量
        sem_destroy(&_dataspace);
        sem_destroy(&_datanum);
    }

private:
    // queue
    vector<T> _ringqueue;
    // 信号量
    // 空间大小
    sem_t _dataspace;
    // 数据个数
    sem_t _datanum;
    // 生产者生产的下标
    uint32_t _proindex;  
    // 消费者消费的下标
    uint32_t _conindex;  
};
