#include "util.hpp"

static void Usage(const string proc)
{
    cout << "Usage:\n\t" << proc << " port [ip]" << endl;
}

class Tcpserver
{
public:
    Tcpserver(uint16_t port, const string &ip = "")
        : _sock(-1), _port(port), _ip(ip)
    {
    }
    ~Tcpserver()
    {
    }

public:
    void init()
    {
        // 1 create cosk
        _sock = socket(AF_INET, SOCK_STREAM, 0);
        if (_sock < 0)
        {
            logMessage(FATAL, "socket: %s%d", strerror(errno), _sock);
            exit(1);
        }
        logMessage(DEBUG, "socket create success : %d", _sock);

        // 2 bind
        // 2.1 填充网络信息
        struct sockaddr_in local;
        memset(&local, 0, sizeof(local));
        local.sin_family = AF_INET;
        local.sin_port = htons(_port);
        // 这里的inet_aton 和 inet_addr的用途一样
        _ip.empty() ? INADDR_ANY : (inet_aton(_ip.c_str(), &local.sin_addr));
        // 2.2 bind网络信息
        if (bind(_sock, (const sockaddr *)&local, sizeof(local)) < 0)
        {
            logMessage(FATAL, "bind: %s%d", strerror(errno), _sock);
            exit(2);
        }
        logMessage(DEBUG, "bind success: %d", _sock);

        // 3 listen
        if (listen(_sock, 5) < 0) // 为什么填5我们后面讲Tcp协议时讲
        {
            logMessage(FATAL, "listen: %s%d", strerror(errno), _sock);
            exit(3);
        }
        logMessage(DEBUG, "listen success: %d", _sock);
    }
    void start()
    {
        while (1)
        {
            // 4 获取连接
            struct sockaddr_in peer;
            socklen_t len = sizeof(peer);
            int servicesock = accept(_sock, (struct sockaddr *)&peer, &len);
            if (servicesock < 0)
            {
                logMessage(WARNING, "accept: %s%d", strerror(errno), servicesock);
                continue;
            }
            logMessage(DEBUG, "accept success: %d", _sock);

            // 4.1 获取客户端信息
            int clientport = ntohs(peer.sin_port);
            string clientip = inet_ntoa(peer.sin_addr);
            // logMessage(DEBUG,"Server run ...");
            // sleep(1);

            // 5 提供服务
            // 5.1 转换大小写
            tranfrom(servicesock, clientip, clientport);
        }
    }
    void tranfrom(int sock, string &ip, int port)
    {
        assert(sock >= 0);
        assert(!ip.empty());
        char inbuffer[1024];
        while (1)
        {
            // 首先要接受信息              
            ssize_t s = read(sock, inbuffer, sizeof(inbuffer) - 1);
            if (s > 0)
            {
                inbuffer[s] = '\0';
                if (strcasecmp(inbuffer, "quit") == 0)
                {
                    logMessage(DEBUG, "clinet quit %s %d", ip.c_str(), port);
                    break;
                }
                logMessage(DEBUG, "Infor : %s %d >>> %s", ip.c_str(), port, inbuffer);

                for (int i = 0; i < s; i++)
                {
                    if (isalpha(inbuffer[i]) && islower(inbuffer[i]))
                    {
                        inbuffer[i] = toupper(inbuffer[i]);
                    }
                }
                write(sock, inbuffer, strlen(inbuffer));
            }
            else if (s == 0)
            {
                // 代表对端关闭，client退出
                logMessage(DEBUG, "clinet quit %s %d", ip.c_str(), port);
                break;
            }
            else
            {
                logMessage(DEBUG, "%s %d read error: %s",ip.c_str(), port, strerror(errno));
                break;
            }
        }

        // client退出会走到这里\
        // 将提供服务的文件描述符关掉
        close(sock);
    }

private:
    int _sock;
    uint16_t _port;
    string _ip;
};

int main(int argc, char *argv[])
{
    if (argc != 2 && argc != 3)
    {
        Usage(argv[0]);
        exit(3);
    }
    uint16_t port = atoi(argv[1]);
    string ip;
    if (argc == 3)
    {
        ip = argv[2];
    }

    Tcpserver tcp(port);
    tcp.init();
    tcp.start();
    return 0;
}