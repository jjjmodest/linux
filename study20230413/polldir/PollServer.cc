#include <iostream>
#include <poll.h>
#include "Sock.hpp"

using namespace std;
#define NUM 1024
struct pollfd fdsArray[NUM]; // 辅助数组 里面存放历史文件描述符
#define DEFAUIT -1           // 默认

void Usage(string process)
{
    cout << "Please entry" << process << " port" << endl;
}

static void ShowArray()
{
    cout << "当前的文件描述符为: ";
    for (int i = 0; i < NUM; i++)
    {
        if (fdsArray[i].fd == DEFAUIT)
            continue;
        cout << fdsArray[i].fd << ' ';
    }
    cout << endl;
}

static void HandlerEvent(int listensock)
{
    for (int j = 0; j < NUM; j++)
    {
        if (fdsArray[j].fd == DEFAUIT)
            continue;
        if (j == 0 && fdsArray[j].fd == listensock)
        {

            if (fdsArray[j].revents & POLLIN)
            {
                cout << "新连接到来，需要处理" << endl;

                string clientip;
                uint16_t clientport = 0;
                int sock = Sock::Accept(listensock, &clientip, &clientport); // 这里不会阻塞
                if (sock < 0)
                    return;
                cout << "获取新连接成功 " << clientip << ":" << clientport << " Sock:" << sock << endl;
                
                int i = 0;
                for (; i < NUM; i++)
                {
                    if (fdsArray[i].fd == DEFAUIT)
                        break;
                }
                if (i == NUM)
                {
                    cerr << "服务器已经到达了上限" << endl;
                    close(sock);
                }
                else
                {
                    // 将文件描述符放入fdsArray中
                    fdsArray[i].fd = sock;
                    fdsArray[i].events = POLLIN;
                    fdsArray[i].revents = 0;
                    // debug
                    ShowArray();
                }
            }
        }
        else
        {
            // 处理其他的文件描述符的IO事件
            if (fdsArray[j].revents & POLLIN)
            {
                char buffer[1024];
                ssize_t s = recv(fdsArray[j].fd, buffer, sizeof(buffer), 0);
                // 这里的阻塞读取真的会阻塞住吗？并不会，因为走到这里select已经帮我们等了，并且此时事件就绪。
                if (s > 0)
                {
                    buffer[s] = 0;
                    cout << "client[" << fdsArray[j].fd << "]"
                         << " # " << buffer << endl;
                }
                else if (s == 0)
                {
                    cout << "client[" << fdsArray[j].fd << "] "
                         << "quit"
                         << " server will close " << fdsArray[j].fd << endl;
                    fdsArray[j].fd = DEFAUIT; // 恢复默认
                    fdsArray[j].events = 0;
                    fdsArray[j].revents = 0;
                    close(fdsArray[j].fd);    // 关闭sock
                    ShowArray();           // debug
                }
                else
                {
                    cerr << "recv error" << endl;
                    fdsArray[j].fd = DEFAUIT; // 恢复默认
                    fdsArray[j].events = 0;
                    fdsArray[j].revents = 0;
                    close(fdsArray[j].fd);    // 关闭sock
                    ShowArray();           // debug
                }
            }
        }
    }
}

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        Usage(argv[0]);
        exit(1);
    }
    int listensocket = Sock::Socket();
    Sock::Bind(listensocket, atoi(argv[1]));
    Sock::Listen(listensocket);

    for (int i = 0; i < NUM; i++)
    {
        fdsArray[i].fd = DEFAUIT;
        fdsArray[i].events = 0;
        fdsArray[i].revents = 0;
    }
    fdsArray[0].fd = listensocket; // 默认fdsArray第一个元素存放
    fdsArray[0].events = POLLIN;
    int timeout = 100000;
    while (1)
    {
        int n = poll(fdsArray, NUM, timeout);
        switch (n)
        {
        case 0:
            // timeout
            cout << "timeout ... : " << (unsigned int)time(nullptr) << endl;
            break;
            // error
            cout << "select error : " << strerror(errno) << endl;
        case -1:
            break;
        default:
            // 等待成功
            HandlerEvent(listensocket);
            break;
        }
    }
}
