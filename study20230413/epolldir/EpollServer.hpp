#pragma once

#include <iostream>
#include <string>
#include <cstdlib>
#include <sys/epoll.h>
#include "log.hpp"
using namespace std;

class EpollServer
{
public:
    static int const gsize = 128;
    EpollServer(uint16_t port)
        : port_(port), listensock_(-1), epfd_(-1)
    {
    }
    void InitEpollServer()
    {
        int sock = Sock::Socket();
        Sock::Bind(sock,port_);
        Sock::Listen(sock);

        epfd_=epoll_create(gsize);
        if(epfd_ < 0)
        {
            logMessage(FATAL,"%d:%s",errno,strerror(errno));
            exit(1);
        }
        logMessage(DEBUG,"epoll_creatr success,epoll模型创建成功");
    }
    void RunServer()
    {

    }

    ~EpollServer()
    {
        if(listensock_ != -1) close(listensock_);
        if(epfd_ != -1) close(epfd_);
    }
private:
    int listensock_;
    int epfd_;
    uint16_t port_;
};