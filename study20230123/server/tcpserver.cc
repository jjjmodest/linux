#include "util.hpp"
#include "tcpserver.hpp"

int main(int argc, char *argv[])
{
    uint16_t port = atoi(argv[1]);
    string ip;
    if (argc == 3)
    {
        ip = argv[2];
    }

    signal(3, sighandler);
    Tcpserver tcp(port, ip);
    svrp = &tcp;
    tcp.init();
    tcp.start();
    return 0;
}