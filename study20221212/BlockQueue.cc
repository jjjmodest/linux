#include "BlockQueue.hpp"
#include "Task.hpp"
// int main()
// {
//     Blockqueue<int> bq;
//     bq.produce(100);
//     int out = bq.consume();
//     cout << out << endl;
//     bq.produce(1100);
//     out = bq.consume();
//     cout << out << endl;
//     bq.produce(1010);
//     out = bq.consume();
//     cout << out << endl;
//     bq.produce(1001);
//     out = bq.consume();
//     cout << out << endl;
//     bq.produce(10011);
//     out = bq.consume();
//     cout << out << endl;
// }

// int main()
// {
//     pthread_t t1, t2, t3;

//     pthread_create(&t1, nullptr, fuc, NULL);
//     pthread_create(&t2, nullptr, fuc, NULL);
//     pthread_create(&t3, nullptr, fuc, NULL);

//     while (1)
//     {
//         char a;
//         cout << "请输入n/q : ";
//         cin >> a;
//         //n为next q为quit
//         if (a == 'n')
//         {
//             //通过条件变量唤醒一个线程
//             pthread_cond_signal(&cond);
//         }
//         else
//         {
//             break;
//         }

//         sleep(0.5);
//     }
//     cout<<"ready to cancel"<<endl;
//     pthread_cancel(t1);
//     pthread_cancel(t2);
//     pthread_cancel(t3);
//     cout<<"cancel success"<<endl;

//     pthread_cond_broadcast(&cond);
//     cout<<"wake all success"<<endl;

//     pthread_join(t1, NULL);
//     pthread_join(t2, NULL);
//     pthread_join(t3, NULL);
//     cout<<"join success"<<endl;

//     pthread_cond_destroy(&cond);
//     pthread_mutex_destroy(&mutex);
//     return 0;
// }
// 条件变量
// pthread_cond_t cond;
// //mutex
// pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
// //线程运行的函数
// void *fuc(void *argc)
// {
//     while (1)
//     {
//         pthread_mutex_lock(&mutex);
//         //等待唤醒
//         pthread_cond_wait(&cond, &mutex);
//         cout << "my thread id : " << pthread_self() << endl;
//         pthread_mutex_unlock(&mutex);
//         break;
//     }
// }

const std::string opera = "+-*/%";

void *concumer(void *queue)
{
    Blockqueue<Task> *bqp = static_cast<Blockqueue<Task> *>(queue);
    while (1)
    {
        sleep(1);
        Task t = bqp->consume();
        int result = t();
        int one, two;
        char opr;
        t.getelem(&one, &two, &opr);
        cout << "Time : " << (unsigned long)time(nullptr) << ' ' << "thread : " << pthread_self()
             << ' ' << "consume data success,data is : " << one << ' ' << opr << ' ' << two << " = " << result << endl;
    }
}
void *producer(void *queue)
{
    Blockqueue<Task> *bqp = static_cast<Blockqueue<Task> *>(queue);
    while (1)
    {
        // 制作数据
        int one = rand() % 20;
        int two = rand() % 10;
        char opr = opera[rand() % opera.size()];
        // 生产数据
        Task t(one, two, opr);
        bqp->produce(t);
        cout << "Time : " << (unsigned long)time(nullptr) << ' ' << "thread : " << pthread_self()
             << ' ' << "produce data success,data is : " << one << ' ' << opr << ' ' << two << " -> ?" << endl;
        sleep(0.5); 
    }
}

int main()
{ // 种随机数种子
    srand((unsigned int)time(nullptr) ^ getpid());

    Blockqueue<Task> bq;
    pthread_t con, prd;

    pthread_create(&con, nullptr, concumer, &bq);
    pthread_create(&prd, nullptr, producer, &bq);

    pthread_join(con, nullptr);
    pthread_join(prd, nullptr);

    return 0;
}
