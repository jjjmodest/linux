#include <iostream>
#include <pthread.h>
#include <unistd.h>
using namespace std;

int count = 0;
pthread_rwlock_t rwmutex;
void *read(void *argc)
{
    char *msg = static_cast<char *>(argc);
    sleep(2);
    while (1)
    {
        pthread_rwlock_rdlock(&rwmutex);
        cout<<"thread : "<<pthread_self()<<" reader read : "<<count<<endl;
        pthread_rwlock_unlock(&rwmutex);
        sleep(1);
    }
}
void *write(void *argc)
{
    char *msg = static_cast<char *>(argc);
    while (1)
    {
        sleep(1);
        pthread_rwlock_wrlock(&rwmutex);
        count++;
        cout<<"writer write : count++"<<endl;
        pthread_rwlock_unlock(&rwmutex);
    }
}

int main()
{

    pthread_t r,r1,r2,r3,r4,r5,w;
    pthread_create(&r, nullptr, read, (void *)"reader");
    pthread_create(&r1, nullptr, read, (void *)"reader");
    pthread_create(&r2, nullptr, read, (void *)"reader");
    pthread_create(&r3, nullptr, read, (void *)"reader");
    pthread_create(&r4, nullptr, read, (void *)"reader");
    pthread_create(&r5, nullptr, read, (void *)"reader");
    pthread_create(&w, nullptr, write, (void *)"writer");

    pthread_join(r, nullptr);
    pthread_join(r1, nullptr);
    pthread_join(r2, nullptr);
    pthread_join(r3, nullptr);
    pthread_join(r4, nullptr);
    pthread_join(r5, nullptr);
    pthread_join(w, nullptr);

    pthread_rwlock_destroy(&rwmutex);

    return 0;
}
