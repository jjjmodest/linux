#include "util.hpp"
#include "protocol.hpp"
volatile bool quit = false;

static void Usage(const string proc)
{
    cout << "Usage:\n\t"
         << "server IP ,server port" << endl;
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        Usage(argv[0]);
        exit(1);
    }
    // 获取服务端
    string serverip = argv[1];
    uint16_t serverport = atoi(argv[2]);

    // 1 创建socket
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0)
    {
        cerr << "socket error : " << strerror(errno) << endl;
        exit(1);
    }

    // 2 connect
    // 2.1 填充
    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(serverip.c_str());
    server.sin_port = htons(serverport);

    // 3 连接
    if (connect(sock, (const struct sockaddr *)&server, sizeof(server)) != 0)
    {
        cerr << "connect error : " << strerror(errno) << endl;
        exit(2);
    }
    cout << "connect sueccess : " << sock << endl;

    // 4 计算
    string message;
    while (!quit)
    {
        message.clear();
        cout << "请输入表达式: ";
        getline(cin, message);
        if (strcasecmp(message.c_str(), "quit") == 0)
        {
            quit = true;
            continue;
        }

        Request req;
        if (!buyRequest(message, &req))
        {
            continue;
        }

        string str;
        req.serialize(&str);
        cout << "message->serialize: " << str << endl;

        str = encode(str, str.size());
        cout << "str->encode: " << str << endl;

        ssize_t s = write(sock, str.c_str(), str.size());
        if (s > 0)
        {
            char buff[1024];
            size_t s = read(sock, buff, sizeof(buff) - 1);
            if (s > 0)
            {
                buff[s] = 0;
                string package = buff;
                cout << "Debug->getmessage: " << package << endl;

                Responce resp;
                uint32_t len = 0;

                string str = decode(package, &len);
                if (len > 0)
                {
                    package = str;
                    cout << "Debug->decode: " << package << endl;
                  
                    resp.Deserialize(package);
                    cout << " result : " << resp._result << " "
                         << " resultcode : " << resp._resultcode << endl;
                }
            }
        }
        else if (s <= 0)
        {
            break;
        }
    }
    close(sock);
    return 0;
}
// #include "util.hpp"
// // 2. 需要bind吗？？需要，但是不需要自己显示的bind！ 不要自己bind！！！！
// // 3. 需要listen吗？不需要的！
// // 4. 需要accept吗？不需要的!

// volatile bool quit = false;

// static void Usage(std::string proc)
// {
//     std::cerr << "Usage:\n\t" << proc << " serverIp serverPort" << std::endl;
//     std::cerr << "Example:\n\t" << proc << " 127.0.0.1 8081\n"
//               << std::endl;
// }
// // ./clientTcp serverIp serverPort
// int main(int argc, char *argv[])
// {
//     if (argc != 3)
//     {
//         Usage(argv[0]);
//         exit(USAGE_ERR);
//     }
//     std::string serverIp = argv[1];
//     uint16_t serverPort = atoi(argv[2]);

//     // 1. 创建socket SOCK_STREAM
//     int sock = socket(AF_INET, SOCK_STREAM, 0);
//     if (sock < 0)
//     {
//         std::cerr << "socket: " << strerror(errno) << std::endl;
//         exit(SOCKET_ERR);
//     }

//     // 2. connect，发起链接请求，你想谁发起请求呢？？当然是向服务器发起请求喽
//     // 2.1 先填充需要连接的远端主机的基本信息
//     struct sockaddr_in server;
//     memset(&server, 0, sizeof(server));
//     server.sin_family = AF_INET;
//     server.sin_port = htons(serverPort);
//     inet_aton(serverIp.c_str(), &server.sin_addr);
//     // 2.2 发起请求，connect 会自动帮我们进行bind！
//     if (connect(sock, (const struct sockaddr *)&server, sizeof(server)) != 0)
//     {
//         std::cerr << "connect: " << strerror(errno) << std::endl;
//         exit(CONN_ERR);
//     }
//     std::cout << "info : connect success: " << sock << std::endl;

//     std::string message;
//     while (!quit)
//     {
//         message.clear();
//         std::cout << "请输入你的消息>>> ";
//         std::getline(std::cin, message); // 结尾不会有\n
//         if (strcasecmp(message.c_str(), "quit") == 0)
//             quit = true;

//         ssize_t s = write(sock, message.c_str(), message.size());
//         if (s > 0)
//         {
//             message.resize(1024);
//             ssize_t s = read(sock, (char *)(message.c_str()), 1024);
//             if (s > 0)
//                 message[s] = 0;
//             std::cout << "Server Echo>>> " << message << std::endl;
//         }
//         else if (s <= 0)
//         {
//             break;
//         }
//     }
//     close(sock);
//     return 0;
// }