#include "head.hpp"
#include <iostream>
using namespace std;

int main()
{
    if(mkfifo(FIFO_PATH,0600) != 0)
    {
        cerr << "mkfifo error"<< endl;
        return 1;
    }
    cout<<"Hello server!"<<endl;
    return 0;   
}