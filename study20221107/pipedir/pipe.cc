#include <iostream>
#include <unistd.h>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <sys/types.h>
#include <sys/wait.h>
#include <vector>
#include <cassert>
#include <unordered_map>
using namespace std;

unordered_map<uint32_t, string> info;

typedef void (*fuc_pipe)();

vector<fuc_pipe> vfus;

void f1()
{
    cout << "自毁程序已启动,执行的进程为:[" << getpid() << "]"
         << "执行时间为:" << time(nullptr) <<endl<<endl;
}
void f2()
{
    cout << "清理人类程序已启动,执行的进程为:[" << getpid() << "]"
         << "执行时间为:" << time(nullptr) <<endl<<endl;
}
void f3()
{
    cout << "屠杀模式已启动,执行的进程为:[" << getpid() << "]"
         << "执行时间为:" << time(nullptr) <<endl<<endl;
}

void loadfuc()
{
    info.insert({vfus.size(), "这是自毁程序"});
    vfus.push_back(f1);
    info.insert({vfus.size(), "这是清理人类程序"});
    vfus.push_back(f2);
    info.insert({vfus.size(), "这是屠杀模式"});
    vfus.push_back(f3);
}


//第一个uint32_t存放：进程pid,第二个uint32_t存放：该进程对应的管道写端fd
typedef pair<uint32_t,uint32_t> gather;
const int ChdProNus = 8;

void Dothing(int fd)
{
    //拿到读端fd
    while(1)
    {
        cout<<"进程：["<<getpid()<<"]准备读取任务"<<endl;
        uint32_t opsb = 0;
        ssize_t sz = read(fd,&opsb,sizeof(uint32_t));
        if(sz == 0)
        {
            break;
        }
        assert(sz == sizeof(opsb));
        (void)sz;

        if(opsb < vfus.size())
        {
            vfus[opsb]();
        }

        //cout<<"进程：["<<getpid()<<"]任务执行结束"<<endl;
    }
}

void DeliveryTasks(const vector<gather> &assignMap)
{
    srand((long long)time(nullptr));
    int cnt = 25;
    while(cnt--)
    {
        sleep(1);
        //select which child process
        uint32_t child = rand() % assignMap.size();
        //select which task
        uint32_t task = rand() %  vfus.size();

        //写入管道
        write(assignMap[child].second,&task,sizeof(task));

        cout<<"派发任务成功"<<endl;
        
    }
}

int main()
{
    vector<gather> assignMap;
    loadfuc();
    //创建ChdProNus个子进程
    for(int i =0; i < ChdProNus; i++)
    {
        //创建管道
        int pipefd[2];
        pipe(pipefd);

        //child read create child
        pid_t pid = fork();
        if(pid == 0)
        {
            //child
            close(pipefd[1]);
            Dothing(pipefd[0]);
            close(pipefd[0]);
            exit(1);
        }

        //parent
        close(pipefd[0]);
        gather g(pid,pipefd[1]);
        assignMap.push_back(g);
    }
    //走到此处只有parent
    //派发任务
    cout<<"准备派发任务"<<endl;
    DeliveryTasks(assignMap);

    //回收资源
    for(int i = 0; i < ChdProNus; i++)
    {
        if(waitpid(assignMap[i].first,nullptr,0))
        {
            cout<<"wait child succes"<<"its pid is :"<<assignMap[i].first<<"it is"<< i <<"th"<<endl;
            close(assignMap[i].second);
        }

    }











    // // 0.load fuc
    // loadfuc();

    // // 1.make pipe
    // int pipefd[2];
    // if (pipe(pipefd) != 0)
    // {
    //     cout << "pipe error" << endl;
    //     exit(1);
    // }

    // pid_t pid = fork();
    // if (pid < 0)
    // {
    //     cout << "fork error" << endl;
    //     exit(2);
    // }
    // else if (pid == 0)
    // {
    //     // child read
    //     close(pipefd[1]);
    //     while (true)
    //     {
    //         //子进程从缓冲区中读
    //         uint32_t opsb = 0;
    //         ssize_t s = read(pipefd[0], &opsb, sizeof(uint32_t));
    //         // 写端关闭，read返回值为0
    //         if (s == 0)
    //         {
    //             cout << "父进程派发任务结束，地球毁灭人类灭亡，子进程退出" << endl;
    //             close(pipefd[0]);
    //             exit(123);
    //         }

    //         assert(s == sizeof(uint32_t));
    //         (void)s;
    //         // assert在debug模式下有效，如果在release模式下无效
    //         //则s变量只被定义未被使用,(void)s 目的是防止warning

    //         //下达任务
    //         if (opsb < vfus.size())
    //         {
    //             vfus[opsb]();
    //         }
    //         else
    //         {
    //             cout << "opsb error" << endl;
    //         }
    //     }

    // }
    // else
    // {
    //     // parent wirte
    //     close(pipefd[0]);

    //     // deliver task through rand
    //     int n = vfus.size();
    //     srand((long long)time(nullptr));

    //     int cnt = 1;
    //     while (cnt != 10)
    //     {
    //         //通过随机数下达命令
    //         uint32_t taskcode = rand() % n;
    //         cout << "父进程正在下达第" << cnt << "次任务，任务是：" << info[taskcode] << endl;
    //         //父进程写入缓冲区
    //         write(pipefd[1], &taskcode, sizeof(uint32_t));
    //         sleep(1);
    //         cnt++;
    //     }
    //     close(pipefd[1]);

    //     // wait child
    //     int status = 0;
    //     pid_t ret = waitpid(pid, &status, 0);
    //     if (ret)
    //     {
    //         cout << "wait child process " <<"exit code :"<<WEXITSTATUS(status)<< endl;
    //     }
    // }

    // return 0;
}