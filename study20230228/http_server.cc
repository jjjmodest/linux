#include "searcher.hpp"
#include "cpp-httplib/httplib.h"

const string root_path = "./wwwroot";
const string raw_path = "data/raw_html/raw.txt";

int main()
{
    httplib::Server svr;

    ns_searcher::Searchear search;
    search.InitSercher(raw_path);

    svr.set_base_dir(root_path.c_str());
    // 创建response
    svr.Get("/s", [&search](const httplib::Request &req, httplib::Response &rsp)
            {
        //rsp.set_content("Hello world!","text/plain: charset=utf-8");
        if(!req.has_param("word")) // 提取参数word
        {
            rsp.set_content("必须要有搜索关键字","text/plain; charset=utf-8");
            return;
        }
        string word = req.get_param_value("word"); // 获取word=的参数
        cout<<"用户搜索的关键字 "<<word<<endl;
        string json_string;
        search.Searcher(word,&json_string); // 根据word调用searher搜索
        rsp.set_content(json_string,"application/json"); });

    svr.listen("0.0.0.0", 9999);
    return 0;
}