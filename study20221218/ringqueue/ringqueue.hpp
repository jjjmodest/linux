#include <iostream>
#include <vector>
#include <ctime>
#include <unistd.h>
#include <semaphore.h>
using namespace std;

const int size = 8;

template <class T>
class Ringqueue
{
public:
    Ringqueue(int cap = size)
        : _ringqueue(cap), _proindex(0), _conindex(0)
    {
        // 初始化信号量
        sem_init(&_dataspace, 0, _ringqueue.size());
        sem_init(&_datanum, 0, 0);
        // 初始化锁 
        pthread_mutex_init(&_pmutex,nullptr);
        pthread_mutex_init(&_cmutex,nullptr);

    }

    void produce(const T &in)
    {
        // 等待信号量，如果有就会使该信号量减减继续如果没有就阻塞
        sem_wait(&_dataspace);
        pthread_mutex_lock(&_pmutex);
        _ringqueue[_proindex] = in;
        // 增加信号量
        _proindex++;
        _proindex %= _ringqueue.size();
        pthread_mutex_unlock(&_pmutex);
        sem_post(&_datanum);

    }
    T consume()
    {
        sem_wait(&_datanum);
        pthread_mutex_lock(&_cmutex);
        T out = _ringqueue[_conindex];
        _conindex++;
        _conindex %= _ringqueue.size();
        pthread_mutex_unlock(&_cmutex);
        sem_post(&_dataspace);
        return out;
    }
    ~Ringqueue()
    {
        // 销毁信号量
        sem_destroy(&_dataspace);
        sem_destroy(&_datanum);
        // 销毁锁
        pthread_mutex_destroy(&_pmutex);
        pthread_mutex_destroy(&_cmutex);
    }

private:
    // queue
    vector<T> _ringqueue;
    // 信号量
    // 空间大小
    sem_t _dataspace;
    // 数据个数
    sem_t _datanum;
    // 生产者生产的下标
    uint32_t _proindex;
    // 消费者消费的下标
    uint32_t _conindex;
    // mutex
    pthread_mutex_t _pmutex;
    pthread_mutex_t _cmutex;
};
