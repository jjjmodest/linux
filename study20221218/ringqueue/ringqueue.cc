#include "ringqueue.hpp"

void *productor(void *args)
{
    Ringqueue<int> *rqp = static_cast<Ringqueue<int> *>(args);
    while (1)
    {
        sleep(1);
        int data = rand() % 199;
        rqp->produce(data);
        cout << "Time : " << (unsigned long)time(nullptr) << ' ' << "thread : " << pthread_self()
             << ' ' << "produce data success,data is : " << data << endl;
    }
}

void *concumer(void *args)
{
    Ringqueue<int> *rqp = static_cast<Ringqueue<int> *>(args);
    while (1)
    {
        int out = rqp->consume();
        cout << "Time : " << (unsigned long)time(nullptr) << ' ' << "thread : " << pthread_self()
             << ' ' << "consume data success,data is : " << out << endl;
    }
}

int main()
{
    srand((unsigned long)time(nullptr) ^ getpid());
    pthread_t c1,c2,c3;
    pthread_t p1,p2,p3;

    Ringqueue<int> rq;
    pthread_create(&c1, nullptr, concumer, &rq);
    pthread_create(&c2, nullptr, concumer, &rq);
    pthread_create(&c3, nullptr, concumer, &rq);
    pthread_create(&p1, nullptr, productor, &rq);
    pthread_create(&p2, nullptr, productor, &rq);
    pthread_create(&p3, nullptr, productor, &rq);

    pthread_join(c1, nullptr);
    pthread_join(c2, nullptr);
    pthread_join(c3, nullptr);
    pthread_join(p1, nullptr);
    pthread_join(p2, nullptr);
    pthread_join(p3, nullptr);
    return 0;
}