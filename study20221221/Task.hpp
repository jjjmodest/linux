#pragma once
#include "iostream"
using namespace std;

class Task
{
public:
    Task(int one = 0, int two = 0, char opr = '0')
        : _elem1(one), _elem2(two), _operator(opr)
    {
    }
    int operator()()
    {
        return calculate();
    }
    int calculate()
    {
        int result = 0;
        switch (_operator)
        {
        case '+':
            result = _elem1 + _elem2;
            break;
        case '-':
            result = _elem1 - _elem2;
            break;
        case '*':
            result = _elem1 * _elem2;
            break;
        case '/':
        {
            if (_elem2 == 0)
            {
                cout << "除零错误" << endl;
                result = INT32_MAX;
            }
            else
            {
                result = _elem1 / _elem2;
            }
        }
        break;
        case '%':
        {
            if (_elem2 == 0)
            {
                cout << "模零错误" << endl;
                result = INT32_MAX;
            }
            else
            {
                result = _elem1 % _elem2;
            }
        }
        break;
        default:
            cout << "错误输入,opertor : " <<'1'<< _operator << endl;
            break;
        }
        return result;
    }
    void getelem(int *one, int *two, char *opr)
    {
        *one = _elem1;
        *two = _elem2;
        *opr = _operator;
    }

private:
    int _elem1;
    int _elem2;
    char _operator;
};