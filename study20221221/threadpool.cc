#include "threadpool.hpp"
#include "Task.hpp"

const std::string opera = "+-*/%";

int main()
{
    unique_ptr<Threadpool<Task>> utp(new Threadpool<Task>());
    utp->start();

    srand((unsigned long)time(nullptr) ^ getpid());
    while (1)
    {
        int one = rand() % 3212;
        int two = rand() % 131;
        char opr = opera[rand()%opera.size()];
        cout<<opr<<endl;
        cout << "Time : " << (unsigned long)time(nullptr) << ' ' << "thread : " << pthread_self() << ' ' << "produce task success,data is : " << one << ' ' << opr << ' ' << two << " -> ?" << endl;
        Task t(one, two, opr);
        utp->push(t);
        sleep(1);
    }
}