#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <cassert>
#include <cstring>
#include "log.hpp"
using namespace std;

static void Usage(const string proc)
{
    cout << "Usage:\n\t"
         << "server IP ,server port" << endl;
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        Usage(argv[0]);
        exit(1);
    }
    // 1. 获取服务端
    string serverip = argv[1];
    uint16_t serverport = atoi(argv[2]);

    //2. 创建客户端
    //2.1 创建socket
    int sockfd = socket(AF_INET,SOCK_DGRAM,0);
    assert(sockfd > 0);

    // 2.2 client 需不需要bind 可以不用bind OS会自动帮我们bind 不推荐自己bind
 
 
    return 0;
}