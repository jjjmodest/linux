#include <iostream>
#include <string>
#include <vector>
#include <boost/filesystem.hpp>
#include <util.hpp>
using namespace boost::filesystem;
using namespace std;

// 存放所有html的文件的路径
const string src_path = "data/input/";
// 存放去除标签化的内容的路径
const string raw_path = "data/raw_html/raw.txt";

typedef struct DocInfo
{
    string title;   // 文档的标题
    string content; // 文档内容
    string url;     // 该文档在官网中的url
} DocInfo_t;

// const & : 输入
//* : 输出
//& ：输入输出

bool EnumFile(const string &src_path, vector<string> *files_list)
{
    path root_path(src_path); // path是类型 root_path通过src_path初始化

    if (!exists(root_path)) // 判断是否root_path存在
    {
        cerr << src_path << "not exists" << endl;
        return false;
    }

    // 定义一个空的迭代器，用来进行判断递归结束
    recursive_directory_iterator end;
    for (recursive_directory_iterator iter(root_path); iter != end; iter++)
    {
        // 从root_path开始递归遍历
        if (!is_regular_file(*iter))
        {
            // 不是常规文件就continue
            continue;
        }

        if (iter->path().extension() != ".html")
        {
            // 后缀不是.html继续continue path()获取路径 extension()获取后缀
            continue;
        }

        // cout << "debug: " << iter->path().string() << endl;
        //  走到这里说明是以.html结尾的网页文件
        files_list->push_back(iter->path().string());
        // 可以将我们对象所对应的路径以字符串的形式传入
        // 将所有的.html文件的路径放入files_list中以便后面的文本分析。
    }
    return true;
}

static bool ParseTitle(const string &file, string *title)
{
    size_t begin = file.find("<title>");
    if (begin == string::npos)
    {
        return false;
    }
    size_t end = file.find("</title>");
    if (end == string::npos)
    {
        return false;
    }

    if (begin > end)
    {
        return false;
    }

    begin += string("<title>").size();

    *title = file.substr(begin, end - begin);
    return true;
}

static bool ParseContent(const string &file, string *content)
{
    // 去标签，基于一个简易的状态机
    enum status
    {
        LABLE,
        CONTENT
    };

    enum status s = LABLE;
    for (char c : file)
    {
        switch (s)
        {
        case LABLE:
        {
            // 如果c字符此时指向的是'>'，标明标签已经被遍历过去了，下一个内容是正文内容
            if (c == '>')
            {
                s == CONTENT;
            }
            break;
        }
        case CONTENT:
        {
            // 如果读到'<'，说明正文读完了，又读到了标签内容
            if (c == '<')
            {
                s = LABLE;
            }
            else
            {
                // 正文
                if (c == '\n')
                {
                    // 不想保留原文件中'\n'，因为我们响应'\n'来作为html解析之后的文本分隔符
                    c = ' ';
                }
                content->push_back(c);
            }
            break;
        }
        default:
            break;
        }
    }

    return true;
}

static bool ParseUrl(const string &file, string *url)
{
    return true;
}

bool ParseHtml(const vector<string> &files_list, vector<DocInfo_t> *results)
{
    // 通过路径找到文件 解析该文件 并提取为DocInfo中的title content url
    for (const string &file : files_list)
    {
        // 1.读取文件，read
        string result;
        if (!ns_util::FileUtil::ReadFile(file, &result))
        {
            // 该函数的作用是将file中的内容读到result里
            // 失败就继续continue
            continue;
        }

        DocInfo_t doc;
        // 2. 提取title
        if (!ParseTitle(result, &doc.title))
        {
            // fail continue
            continue;
        }

        // 3. 提取content
        if (!ParseContent(result, &doc.content))
        {
            // fail continue
            continue;
        }

        // 4. 提取url
        if (!ParseUrl(result, &doc.url))
        {
            // fail continue
            continue;
        }

        // 走到这里内容都被存放到了doc中
        results->push_back(doc); // 这里实际上是拷贝效率较低后面我们在作出修改
    }

    return true;
}

bool SaveHtml(const vector<DocInfo_t> &results, const string &raw_path)
{

    return true;
}

int main()
{
    vector<string> files_list;
    // 1. 通过该函数递归式的将每个html文件带路径，保存到files_list中。
    if (!EnumFile(src_path, &files_list))
    {
        cerr << "EnumFile error" << endl;
        return 1;
    }

    // 2. 通过files_list中的内容进行解析
    vector<DocInfo_t> results;
    if (!ParseHtml(files_list, &results))
    {
        cerr << "ParseHtml error" << endl;
        return 2;
    }

    // 3. 把解析完毕的各个文件内容写入到raw_path中，按照"\3"作为每个文档内容之间的分隔符
    if (!SaveHtml(results, raw_path))
    {
        cerr << "SaveHtml error" << endl;
        return 3;
    }

    return 0;
}
