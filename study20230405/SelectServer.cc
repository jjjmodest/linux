#include <iostream>
#include <sys/select.h>
#include "Sock.hpp"

using namespace std;

int fdsArray[sizeof(fd_set) * 8] = {0}; // 辅助数组 里面存放历史文件描述符
const int gnum = sizeof(fdsArray) / sizeof(fdsArray[0]);
#define DEFAUIT -1 // 默认

void Usage(string process)
{
    cout << "Please entry" << process << " port" << endl;
}

static void ShowArray()
{
    cout << "当前的文件描述符为: "; 
    for(int i = 0; i < gnum; i++)
    {
        if(fdsArray[i] == DEFAUIT)
            continue;
        cout << fdsArray[i] << ' ';
    }
    cout<<endl;
}

static void HandlerEvent(int listensock, fd_set &readfds)
{
    if (FD_ISSET(listensock, &readfds))
    {
        // 使用FD_ISSET来判断该listensock对应的读事件就绪
        // 走到这里，说明listensock对应的读事件就绪，或者说来了一个新链接
        cout << "新连接到来，需要处理" << endl;

        string clientip;
        uint16_t clientport = 0;
        int sock = Sock::Accept(listensock, &clientip, &clientport); // 这里不会阻塞
        if (sock < 0)
            return;
        cout << "获取新连接成功 " << clientip << ":" << clientport << " Sock:" << sock << endl;

        // 将sock放入readfds中,首先要将sock放入辅助数组中
        int i = 0;
        for (; i < gnum; i++)
        {
            if(fdsArray[i] == DEFAUIT) break;
        }
        if(i == gnum)
        {
            // 说明文件描述符已经占满了fdsArray,新的文件描述符放入不到fdsArray中,fdsArray大小是fd_set的大小
            // 说明服务器已经到达了上限,等待不了新的文件描述符
            cerr << "服务器已经到达了上限"<<endl;
            close(sock);
        }
        else
        {
            // 将文件描述符放入fdsArray中
            fdsArray[i] = sock;
            // debug
            ShowArray();
        }

    }
}

int main(int argc, char **argv)
{
    // fd_set set;
    // cout<<sizeof(set)*8<<endl;
    if (argc != 2)
    {
        Usage(argv[0]);
        exit(1);
    }
    int listensocket = Sock::Socket();
    Sock::Bind(listensocket, atoi(argv[1]));
    Sock::Listen(listensocket);

    for (int i = 0; i < gnum; i++)
    {
        fdsArray[i] = DEFAUIT;
    }
    fdsArray[0] = listensocket; // 默认fdsArray第一个元素存放
    while (1)
    {
        // 在每次select的时候,将参数重新设定
        int maxfd = DEFAUIT;
        fd_set readfds;
        FD_ZERO(&readfds);
        for (int i = 0; i < gnum; i++)
        {
            if (fdsArray[i] == DEFAUIT)
                continue;
            // 走到这里说明fdsArray[i]，存放的是文件描述符，不是DEFAUIT 则需要将这个文件描述符添加入readfds中
            FD_SET(fdsArray[i], &readfds);
            if (fdsArray[i] > maxfd)
                maxfd = fdsArray[i]; // 更新maxfd
        }
        struct timeval timeout = {50, 0};
        // 这里将设置的初始化的参数放入循环中，是因为先前讲过
        // select中的readfds等参数是输入输出型参数，一旦timeout，也就是
        // 在规定时间内，或者有其他在位图中的文件描述符对应的读事件就绪了，readfds就会
        // 被清空，只设置就绪的文件描述符对应的位图。
        // 例子：传入0110 1111，第2个读事件就绪，返回0000 0100，但是其他文件描述符需要继续关注，所以要重复设置。
        // 我们当前只关注读事件就绪 listensocket永远要设置进readfds中，时刻在监听
        int n = select(maxfd + 1, &readfds, nullptr, nullptr, &timeout);
        switch (n)
        {
        case 0:
            // timeout
            cout << "timeout ... : " << (unsigned int)time(nullptr) << endl;
            break;
            // error
            cout << "select error : " << strerror(errno) << endl;
        case -1:
            break;
        default:
            // 等待成功
            HandlerEvent(listensocket, readfds);
            break;
        }
    }
}
