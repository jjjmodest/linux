#include <iostream>
using namespace std;


void fuc(int a)
{
    cout<< a+1<<endl;
}
void fuc(int64_t a)
{
    cout<< a+2<<endl;
}

int main()
{
    int *a = nullptr;
    fuc(1);
}
class Solution {
public:
    vector<int> GetLeastNumbers_Solution(vector<int> input, int k) 
    {
        vector<int> ret;
        priority_queue<int> pq;
        if(k == 0 || k > input.size())
            return ret;
        for(int i = 0; i < input.size(); i++)
        {
            if(pq.size() < k)
            {
                pq.push(input[i]);
            }
            else
            {
                if(pq.top() > input[i])
                {
                    pq.pop();
                    pq.push(input[i]);
                }
            }
        } 

        while(!pq.empty())
        {
            ret.push_back(pq.top());
            pq.pop();
        }
        return ret;
    }
};