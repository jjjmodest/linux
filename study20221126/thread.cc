#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <cstring>
using namespace std;

void *fuc(void *argc)
{
    sleep(1);
    pthread_detach(pthread_self());
    int cnt = 5;//让线程自动退出
    while (cnt--)
    {
        cout << "我的tid为 " << pthread_self() << endl;
        sleep(1);
    }
    // char *msg = static_cast<char *>(argc);
    // cout<<11<<endl;
}

int main()
{
    pthread_t tid;
    pthread_create(&tid, NULL, fuc, (void *)"thread 1");
    // sleep(1); //为了先让线程运行
    cout << "我是主线程" << endl;
    int n = pthread_join(tid, NULL);
    if (n == 0)
    {
        cout << "join success!" << endl;
    }
    else
    {
        cout << n << "::" << strerror(n) << endl;
    }

    sleep(10); //为了避免主线程退出导致其他对等线程退出
    return 0;
}

// int a = 1;

// void *fuc(void *argc)
// {
//     int cnt = 3;
//     while (cnt--)
//     {
//         cout << "我是线程1" << endl;
//         sleep(1);
//     }
//     int *p = new int(10);
//     return (void *)p;
//     // char *msg = (char *)(argc);
//     //   int cnt = 100000;

//     // while (cnt--)
//     // {
//     //     a++;
//     //     cout << "我是 : " << msg << endl;
//     // }
//     //pthread_exit((void *)1);
// }
// void *fuc1(void *argc)
// {

//     char *msg = (char *)(argc);
//     int cnt = 100000;

//     while (cnt--)
//     {
//         a++;
//         cout << "我是 : " << msg << endl;
//     }
// }
// void *fuc2(void *argc)
// {

//     char *msg = (char *)(argc);
//     int cnt = 100000;
//     while (cnt--)
//     {
//         a++;
//         cout << "我是 : " << msg << endl;
//     }
// }
// int main()
// {
//     // pthread_t tid;
//     // pthread_create(&tid, NULL, fuc, NULL);
//     // void *retval;
//     // int ret = pthread_join(tid, &retval);
//     // cout << (long long)retval << endl;
//     // // PTHREAD_CANCELED;

//     pthread_t tid;
//     pthread_create(&tid, NULL, fuc, NULL);
//     void *retval;
//     sleep(3);
//     //pthread_cancel(tid);
//     int ret = pthread_join(tid, &retval);
//     cout<<ret<<endl;
//     cout<<*(int*)retval<<endl;

//     // cout<<**retval<<endl;

//     // pthread_t pid;
//     // pthread_t pid1;
//     // pthread_t pid2;
//     // pthread_create(&pid, NULL, fuc, (void *)"pthread 1");
//     // pthread_create(&pid1, NULL, fuc1, (void *)"pthread 2");
//     // pthread_create(&pid2, NULL, fuc2, (void *)"pthread 2");
//     // while (1)
//     // {
//     //     cout << a << endl;
//     // }
//     // pthread_join(pid, NULL);
//     // pthread_join(pid1, NULL);
//     // pthread_join(pid2, NULL);
//     // return 0;
// }