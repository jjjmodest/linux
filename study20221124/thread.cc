#include <iostream>
#include <unistd.h>
#include <pthread.h>
using namespace std;

void *fuc(void *a)
{
    char *msg = static_cast<char *>(a);
    while (1)
    {
        cout << "我是" << msg << "我的pid :" << getpid() << endl;
        sleep(1);
    }
}

void *fuc1(void *a)
{
    char *msg = static_cast<char *>(a);
    while (1)
    {
        cout << "我是" << msg << "我的pid :" << getpid() << endl;
        sleep(1);
    }
}

void *fuc2(void *a)
{
    char *msg = static_cast<char *>(a);
    while (1)
    {
        cout << "我是" << msg << "我的pid :" << getpid() << endl;
        sleep(1);
    }
}

int main()
{
    pthread_t tid, tid1, tid2;

    pthread_create(&tid, NULL, fuc, (void *)"thread");
    pthread_create(&tid1, NULL, fuc1, (void *)"thread1");
    pthread_create(&tid2, NULL, fuc2, (void *)"thread2");

    while (1)
    {
        cout << "我是主线程,我的pid :" << getpid() << endl;
        sleep(1);
    }

    pthread_join(tid, NULL);
    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);

    return 0;
}