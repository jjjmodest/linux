#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
using namespace std;

void fuc(int sig)
{
    cout << "I got a signal,it is " << sig << endl;
    exit(1);
}

int main()
{
    for (int sig = 1; sig <= 31; sig++)
    {
        signal(sig, fuc);
    }
    int arr[10] = 0;
    arr[111] = 10;
    // while (1)
    // {
    //     cout << "Doing" << endl;
    //     kill(getpid(), 9);
    // }
    return 0;
}