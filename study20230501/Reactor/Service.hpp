#pragma once 

#include "Protocol.hpp"
#include <functional>

using service_t = function<Response (Request &req)>;


Response Calculate(Request &req)
{

    Response resp = {0, 0};
    switch (req._op)
    {
    case '+':
        resp._result = req._x + req._y;
        break;
    case '-':
        resp._result = req._x - req._y;
        break;
    case '*':
        resp._result = req._x * req._y;
        break;
    case '/':
    {
        if (req._y == 0)
        {
            resp._exitcode = 1; // 1 除零错误
            resp._result = INT32_MAX;
        }
        else
            resp._result = req._x / req._y;
        break;
    }
    case '%':
    {
        if (req._y == 0)
        {
            resp._exitcode = 2; // 2 模零错误
            resp._result = INT32_MAX;
        }
        else
            resp._result = req._x % req._y;
        break;
    }
    default:
        resp._exitcode = 3; // 非法输入
        break;
    }
    return resp;
}






