#include "Tcpserver.hpp"
#include "Sock.hpp"
#include "Protocol.hpp"
#include "Service.hpp"
#include <memory>

using namespace std;

void Usage(string process)
{
    cout << "Please entry" << process << " port" << endl;
}

int HandlerProHelp(Connection *conn, string &message,service_t service)
{
// 我们能保证走到这里一定是完整的报文，已经解码
    cout << "---------------" << endl;
    // 1 * 1
    // 接下来是反序列化
    cout << "获取request : " << message << endl;
    Request req;
    if (Parser(message, &req) == false)
    {
        return -1;
    }

    // 业务处理
    Response resp = service(req);

    // 序列化
    string out;
    Serialize(resp, &out);

    // 发送给client
    conn->_outbuff += out;
    conn->_writefuc(conn);
    if (conn->_outbuff.empty())
    {
        if (conn->_outbuff.empty() == 0)
            conn->_ptr->ModSockEvent(conn->_sock, true, false);
        else
            conn->_ptr->ModSockEvent(conn->_sock, true, true);
    }
    // // 发送
    // // conn->_ptr->ModSockEvent(conn->_sock, true, true);

    cout << "---------------" << endl;

    return 0;
}


int HandlerPro(Connection *conn, string &message)
{
    return HandlerProHelp(conn,message,Calculate);
}

int main(int argv, char **argc)
{
    if (argv != 2)
    {
        Usage(argc[0]);
        exit(1);
    }
    unique_ptr<Tcpserver> ep(new Tcpserver(HandlerPro, atoi(argc[1])));
    ep->Run();
    return 0;
}