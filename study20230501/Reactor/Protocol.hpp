#pragma once

#include <iostream>
#include <vector>
#include <string>
using namespace std;

#define SEP 'X'
#define SEP_SZ sizeof(SEP)

#define CRLF "\r\n"
#define CRLF_LEN strlen(CRLF)
#define SPACE " "
#define SPACE_LEN strlen(SPACE)

void PackageSplit(string &buff, vector<string> *result)
{
    // asdasXdasdaXda
    // asdas dasda da
    while (true)
    {
        size_t pos = buff.find(SEP);
        if (pos == string::npos)
        {
            if (buff.size() < 5)
            {
                buff.clear();
                break;
            }
            result->push_back(buff.substr(0, buff.size()));
            buff.clear();
            break;
        }
        result->push_back(buff.substr(0, pos));
        buff.erase(0, pos + SEP_SZ);
    }
}

struct Request
{
    int _x;
    int _y;
    char _op;
};

struct Response
{
    int _result;
    int _exitcode;
};

bool Parser(string &in, Request *out)
{
    // 反序列化
    // 1 + 1, 2 * 4, 5 * 9, 6 *1
    std::size_t spaceOne = in.find(SPACE);
    if (std::string::npos == spaceOne)
        return false;
    std::size_t spaceTwo = in.rfind(SPACE);
    if (std::string::npos == spaceTwo)
        return false;

    std::string dataOne = in.substr(0, spaceOne);
    std::string dataTwo = in.substr(spaceTwo + SPACE_LEN);
    std::string oper = in.substr(spaceOne + SPACE_LEN, spaceTwo - (spaceOne + SPACE_LEN));
    if (oper.size() != 1)
        return false;

    // 转成内部成员
    out->_x = atoi(dataOne.c_str());
    out->_y = atoi(dataTwo.c_str());
    out->_op = oper[0];

    return true;
}

void Serialize(Response &in, string *out)
{
    // 序列化
    // "exitCode_ result_"
    std::string ec = std::to_string(in._exitcode);
    std::string res = std::to_string(in._result);

    *out = ec;
    *out += SPACE;
    *out += res;
    *out += CRLF;
}
