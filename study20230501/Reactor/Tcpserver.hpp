#pragma once

#include <iostream>
#include <string>
#include <unordered_map>
#include <cstdlib>
#include <sys/epoll.h>
#include <cassert>
#include <functional>
#include "Sock.hpp"
#include "Log.hpp"
#include "Epoller.hpp"
#include "Util.hpp"
#include "Protocol.hpp"
#include "Epoller.hpp"
using namespace std;

class Connection;
class Tcpserver;
using fuc_t = function<int(Connection *)>;
using callbcak_t = function<int(Connection *, string &)>;

class Connection
{

public:
    // 文件描述符
    int _sock;

    Tcpserver *_ptr;
    //  自己的接受和发送缓冲区
    string _inbuff;
    string _outbuff;

    // 回调函数
    fuc_t _readfuc;
    fuc_t _writefuc;
    fuc_t _exceptfuc;

public:
    Connection(int sock, Tcpserver *ptr) : _sock(sock), _ptr(ptr)
    {
    }
    ~Connection()
    {
    }
    void SetReadfuc(fuc_t fuc)
    {
        _readfuc = fuc;
    }
    void SetWritefuc(fuc_t fuc)
    {
        _writefuc = fuc;
    }
    void SetExceptfuc(fuc_t fuc)
    {
        _exceptfuc = fuc;
    }
};

class Tcpserver
{
public:
    Tcpserver(callbcak_t cb, int port) : _cb(cb)
    {

        // 网络
        _listensock = Sock::Socket();
        Util::SetNonBlock(_listensock);
        Sock::Bind(_listensock, port);
        Sock::Listen(_listensock);

        // epoll
        _epfd = Epoller::CreateEpoll();

        //  添加listen事件
        AddConn(_listensock, EPOLLIN | EPOLLET,
                std::bind(&Tcpserver::Accepter, this, std::placeholders::_1), nullptr, nullptr);

        // // add事件
        // Epoller::Addevent(_epfd, _listensock, EPOLLIN | EPOLLET);

        // // 将listensock匹配的connection方法添加到unordered_map中
        // auto iter = new Connection(_listensock, this);
        // iter->SetReadfuc(std::bind(&Tcpserver::Accepter, this, std::placeholders::_1));
        // _conn.insert({_listensock, iter});

        // 初始化就绪队列
        _revs = new struct epoll_event[_revs_num];
    }
    int Accepter(Connection *conn)
    {
        while (1)
        {
            string clientip;
            uint16_t clientport;
            int sockfd = Sock::Accept(conn->_sock, &clientip, &clientport);
            if (sockfd < 0)
            {
                if (errno == EINTR) // 被信号中断
                    continue;
                else if (errno == EAGAIN || errno == EWOULDBLOCK) // 读取结束
                    break;
                else
                {
                    // 出错
                    logMessage(FATAL, "accept error");
                    return -1;
                }
            }
            logMessage(DEBUG, "Get a new connect : %d", sockfd);
            AddConn(sockfd, EPOLLIN | EPOLLET,
                    std::bind(&Tcpserver::TcpRecver, this, std::placeholders::_1),
                    std::bind(&Tcpserver::TcpSender, this, std::placeholders::_1),
                    std::bind(&Tcpserver::TcpExcepter, this, std::placeholders::_1));
        }
        return 0;
    }
    bool SockinConn(int sock)
    {
        auto iter = _conn.find(sock);
        if (iter == _conn.end())
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    void AddConn(int sock, uint32_t event, fuc_t readfuc, fuc_t writefuc, fuc_t exceptfuc)
    {
        if (event & EPOLLET)
            Util::SetNonBlock(sock);

        // 将文件描述符加入epoll模型中
        Epoller::Addevent(_epfd, sock, event);
        // 将文件描述符匹配的connection，也加入map中
        Connection *conn = new Connection(sock, this);

        conn->SetReadfuc(readfuc);
        conn->SetWritefuc(writefuc);
        conn->SetExceptfuc(exceptfuc);

        // conn->SetReadfuc(std::bind(&Tcpserver::TcpRecver, this, std::placeholders::_1));
        // conn->SetWritefuc(std::bind(&Tcpserver::TcpSender, this, std::placeholders::_1));
        // conn->SetExceptfuc(std::bind(&Tcpserver::TcpExcepter, this, std::placeholders::_1));

        _conn.insert({sock, conn});
        logMessage(DEBUG, "将文件描述符匹配的connection加入map成功");
    }

    int TcpRecver(Connection *conn)
    {
        // 对普通套接字读取
        while (true)
        {
            char buff[1024];
            ssize_t sz = recv(conn->_sock, buff, sizeof(buff) - 1, 0);
            if (sz > 0)
            {
                buff[sz] = '\0';
                conn->_inbuff += buff;
            }
            else if (sz == 0)
            {
                conn->_exceptfuc(conn);
                logMessage(DEBUG, "client quit");
                break;
            }
            else if (sz < 0)
            {
                if (errno == EINTR)
                {
                    // 因为信号导致IO关闭，但数据还没有读完
                    continue;
                }
                else if (errno == EAGAIN || errno == EWOULDBLOCK)
                {
                    // 读完了
                    break;
                }
                else
                {
                    // 读取出错
                    logMessage(FATAL, "recv error errno:%d : %s", errno, strerror(errno));
                    conn->_exceptfuc(conn);
                    break;
                }
            }
        }
        // 本轮读取完毕
        // 将读取上来的 如：xxxxx/3xxxxx/3xxx/3
        // 分为 xxxxx 、xxxxx、xxx
        vector<string> result;
        PackageSplit(conn->_inbuff, &result);
        for (auto &message : result)
        {
            _cb(conn, message);
        }
        return 0;
    }

    int TcpSender(Connection *conn)
    {
        // 对普通套接字发送
        while (1)
        {
            ssize_t sz = send(conn->_sock, conn->_outbuff.c_str(), conn->_outbuff.size(), 0);
            if (sz > 0)
            {
                // 去除已成功发送的数据
                conn->_outbuff.erase(0, sz);
            }
            else
            {
                if (errno == EINTR)
                    continue;
                else if (errno == EAGAIN || errno == EWOULDBLOCK)
                    // 到了此处，不一定就是outbuff中的数据都发完了，有可能内核缓冲区已经满了 继续保持EPOLLOUT打开
                    break;
                else
                {
                    logMessage(FATAL, "send error errno:%d : %s", errno, strerror(errno));
                    conn->_exceptfuc(conn);
                    break;
                }
            }
        }

        if (conn->_outbuff.empty() == 0)
            ModSockEvent(conn->_sock, true, false);
        else
            ModSockEvent(conn->_sock, true, true);
    }

    int TcpExcepter(Connection *conn)
    {
        // 处理普通套接字异常
        // 0.检测
        if (!SockinConn(conn->_sock))
            return -1;

        // 1.移除事件
        Epoller::DelEvent(_epfd, conn->_sock);
        logMessage(DEBUG, "remove epoll event");

        // 2.关闭文件描述符
        close(conn->_sock);
        logMessage(DEBUG, "close fd :%d", conn->_sock);

        // 3.删除map中的sock对应的conn
        // delete conn;
        delete _conn[conn->_sock];
        logMessage(DEBUG, "delete conn object success");

        // 4.去掉sock和conn的映射关系 上一步只是delete掉了对象，但是映射关系还在
        _conn.erase(conn->_sock);
        logMessage(DEBUG, "erase conn from map success");
    }

    void Dispatcher()
    {
        // 获取就绪事件
        int n = Epoller::GetReadyFd(_epfd, _revs, _revs_num);
        // logMessage(DEBUG, "GetReadyFd,epoll_wait");
        // 事件派发
        for (int i = 0; i < n; i++)
        {
            int sock = _revs[i].data.fd;
            uint32_t revent = _revs[i].events;
            //  转为读写事件就绪
            if (EPOLLERR & revent)
                revent |= (EPOLLIN | EPOLLOUT); // 出错
            if (EPOLLHUP & revent)
                revent |= (EPOLLIN | EPOLLOUT); // 文件描述符关闭
            if (EPOLLIN & revent)
            {
                // 先判空
                if (SockinConn(sock) && _conn[sock]->_readfuc)
                {
                    // 该文件描述符对应的读方法
                    _conn[sock]->_readfuc(_conn[sock]);
                }
            }
            if (EPOLLOUT & revent)
            {
                // 先判空
                if (SockinConn(sock) && _conn[sock]->_writefuc)
                {
                    // 该文件描述符对应的写方法
                    _conn[sock]->_writefuc(_conn[sock]);
                }
            }
        }
    }
    // 打开或关闭sock对应的写或者读事件
    void ModSockEvent(int sock, bool read, bool write)
    {
        uint32_t event = 0;
        event |= read ? EPOLLIN : 0;
        event |= write ? EPOLLOUT : 0;
        Epoller::ModEvent(_epfd, sock, event);
    }

    void Run()
    {
        while (1)
        {
            Dispatcher();
        }
    }

    ~Tcpserver()
    {
        if (_listensock != -1)
            close(_listensock);
        if (_epfd != -1)
            close(_epfd);
        delete[] _revs;
    }

private:
    // 1.网络sock
    int _listensock;
    // 2.epoll
    int _epfd;
    // 3.将epoll与上层代码结合
    unordered_map<int, Connection *> _conn;
    // 4.就绪事件列表
    struct epoll_event *_revs;
    // 5.就绪事件列表大小
    static const int _revs_num = 64;
    // 6.设置完整报文的处理方法
    callbcak_t _cb;
};
