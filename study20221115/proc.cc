#include <iostream>
#include <cstring>
#include <string>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

using namespace std;

void fuc(int sig)
{
    cout << "获取信号成功，获取的信号为: " << sig << "已调用处理信号程序" << endl;
}

int main(int argc, char *argv[])
{
    int cnt = 10;
    while(cnt--)
    {
        cout<<"运行ing"<<endl;
        sleep(1);
    }
    abort();
    // signal(2,fuc);
    // while (1)
    // {
    //     raise(2);
    //     cout<<"已发送信号"<<endl;
    //     sleep(1);
    // }

    // //传入参数为 ./当前进程名字|要被杀掉的进程名字|进程pid|信号
    // if (kill(static_cast<pid_t>(atoi(argv[2])), atoi(argv[3])) == -1)
    // {
    //     cout << "kill error" << strerror(errno) << endl;
    //     exit(1);
    // }
    // else
    // {
    //     cout << "kill success,proc: " << argv[1] << " pid: " << argv[2] << endl;
    // }
    // for (int sig = 1; sig <= 31; sig++)
    // {
    //     signal(sig, fuc);
    // }
    // sleep(3);
    // cout << "已设置处理信号程序" << endl;
    // sleep(3);
    // while (1)
    // {
    //     cout << "请传入SIGINT信号，查看调用结果" << endl;
    //     sleep(1);
    // }
    // return 0;
}