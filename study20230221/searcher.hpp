#pragma once
#include "index.hpp"
#include "util.hpp"
#include <algorithm>
#include <jsoncpp/json/json.h>
namespace ns_searcher
{
    class Searchear
    {
    private:
        ns_index::Index *index; // 供系统进行查找的索引
    public:
        Searchear() {}
        ~Searchear() {}

    public:
        void InitSercher(const string &input)
        {
            // 初始化索引
            index = ns_index::Index::GetInstance();
            index->BuildIndex(input);
        }

        // query 搜索关键字
        // json_string 返回给用户浏览器的数据搜索结果
        void Searcher(const string &query, string *json_string)
        {
            // 1.[分词]:对我们的querry进行按照searcher的要求进行分词
            vector<string> words;
            ns_util::JiebaUtil::CutString(query, &words);

            // 2.[触发]:就是根据分词的各个"词"，进行index查找
            ns_index::InvertedList inverted_list_all;
            for (auto &ch : words)
            {
                boost::to_lower(ch);

                ns_index::InvertedList *inverted_list = index->GetInvertedList(ch);
                if (inverted_list == nullptr)
                {
                    continue;
                }
                inverted_list_all.insert(inverted_list_all.end(), inverted_list.begin(), inverted_list.end());
            }
            // 3.[合并排序]:根据weight相关性进行降序排序
            sort(inverted_list_all.begin(), inverted_list_all.end(), [](const ns_index::InvertedElem &e1, const ns_index::InvertedElem &e2)
                 { return e1.weight > e2.weight; });
            // 4.[构建]:根据查找出来的结果,构建json串--jsoncpp--通过jsoncpp完成序列化与反序列化的内容
            // 关于序列化与反序列化的内容我先前的博客也讲述过
            Json::Value root;
            for (auto &ch : inverted_list_all)
            {
                ns_index::DocInfo_t *doc = index->GetforwardIndex(ch.doc_id);
                if (doc == nullptr)
                {
                    continue;
                }
                Json::Value elem;
                elem["title"] = doc->title;
                elem[""] = doc->content; // 我们此时想要的是摘要，我们搜索出来的一系列网站要有 标题 摘要 url 这里我们放到最后解决
                elem["url"] = doc->url;

                root.append(elem);
            }
            Json::StyledWriter writer;
            *json_string = writer.write(root);
        }
    };
}
