#pragma once
#include "util.hpp"

#define CRLF "\r\n"
#define CRLFLEN strlen(CRLF)
#define SPACE " "
#define SPACELEN strlen(SPACE)

#define OPS "+-*/%"

// encode 为整个序列化之后的字符串添加长度
string encode(const string &in, uint32_t len)
{
    // "_resultcode _result" -> "len\r\n_resultcode _result\r\n"
    string ret = to_string(len);
    ret += CRLF;
    ret += in;
    ret += CRLF;
    return ret;
}
// 1 可以作为检查函数，检查有没有读取到完整序列化并encode过的字符串
//  必须有完整长度  具有与长度相符的有效载荷
// 2 由上述条件可以返回有效载荷和有效长度
// decode 为整个序列化之后的字符串提取长度
//   9\r\n888 + 666\r\n \r\n88981\n
string decode(string &in, uint32_t *len)
{
    assert(len);
    // 初始检查
    *len = 0;
    size_t pos = in.find(CRLF);
    if (pos == string::npos)
        return "";

    // 1 提取长度
    string slen = in.substr(0, pos);
    int ilen = atoi(slen.c_str());

    // 2 确认有效载荷是否符合要求
    int sz = in.size() - 2 * CRLFLEN - pos;
    if (sz < ilen)
        return "";

    // 3 提取888 + 666
    string package = in.substr(pos + CRLFLEN, ilen);
    *len = ilen;

    // 4 将提取完毕的字符串从读取的字符串删除便于下次decode
    int remveLen = slen.size() + 2 * CRLFLEN + package.size();
    in.erase(0, remveLen);

    // 5 返回
    return package;
}

// 定制请求
class Request
{
public:
    Request()
    {
    }
    ~Request()
    {
    }
    // 序列化
    void serialize(string *out)
    {
        string x = to_string(_x);
        string y = to_string(_y);

        *out = x;
        *out += SPACE;
        *out += _opr;
        *out += SPACE;
        *out += y;
    }
    // 反序列化
    bool Deserialize(const string &in)
    {
        // 888 + 666
        size_t firpos = in.find(SPACE);
        if (firpos == string::npos)
            return false;
        size_t secpos = in.rfind(SPACE);
        if (secpos == string::npos)
            return false;

        string date1 = in.substr(0, firpos);
        string date2 = in.substr(secpos + SPACELEN);
        string opr = in.substr(firpos + SPACELEN, secpos - (firpos + SPACELEN));
        if (opr.size() != 1)
            return false;

        _x = atoi(date1.c_str());
        _y = atoi(date2.c_str());
        _opr = opr[0];
        return true;
    }
    void debug()
    {
        cout << "---------------------------------------------" << endl;
        cout << " _x: " << _x << " _opr: " << _opr << " _y: " << _y<<endl;
        cout << "---------------------------------------------" << endl;
    }

public:
    int _x;
    char _opr;
    int _y;
};

// 定制响应
class Responce
{
public:
    Responce() : _resultcode(0), _result(0)
    {
    }
    ~Responce()
    {
    }
    // 序列化
    void serialize(string *out)
    {
        // "_resultcode _result"
        string code = to_string(_resultcode);
        string res = to_string(_result);

        *out = code;
        *out += SPACE;
        *out += res;
    }
    // 反序列化
    bool Deserialize(const string &in)
    {
        // "_resultcode _result"
        size_t pos = in.find(SPACE);
        if (pos == string::npos)
        {
            return false;
        }
        string rcode = in.substr(0, pos);
        string result = in.substr(pos + SPACELEN);
        _resultcode = atoi(rcode.c_str());
        _result = atoi(result.c_str());

        return true;
    }
    void debug()
    {
        cout << "---------------------------------------------" << endl;
        cout << " result: " << _result << " resultcode: " << _resultcode << endl;
        cout << "---------------------------------------------" << endl;
    }

public:
    int _resultcode;
    int _result;
};

bool buyRequest(string &msg, Request *req)
{
    // 将1+1输入的字符串存储到对象中
    char buff[1024];
    snprintf(buff, sizeof(buff), "%s", msg.c_str());
    char *left = strtok(buff, OPS);
    if (left == nullptr)
    {
        return false;
    }
    char *right = strtok(nullptr, OPS);
    if (right == nullptr)
    {
        return false;
    }
    char mid = msg[strlen(left)];

    req->_x = atoi(left);
    req->_y = atoi(right);
    req->_opr = mid;

    return true;
}