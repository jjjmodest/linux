#pragma once

#include <iostream>
#include <queue>
#include <unistd.h>
#include <cstdlib>

using namespace std;

template <class T>
class Blockqueue
{
public:
    // 生产
    void produce()
    {
        // 加锁
        // 判断是否有条件进行生产
        // 解锁
    }
    // 消费
    void consume()
    {
        // 加锁
        // 判断是否有条件进行消费
        // 解锁
    }
private:
    // 队列
    queue<T> _q;
    // size
    uint32_t _capacity;
    // mutex
    pthread_mutex_t _mutex;
    //让消费者等待的条件变量
    pthread_cond_t _concond;
    //让生产者等待的条件变量
    pthread_cond_t _prtcond;
};
