#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>


int main()
{
  pid_t a  = fork();
  if(a == 0)
  {
    while(1)
    {
      printf("我是子进程，我正在运行。。。我的pid是%d\n",getpid());
      sleep(1);
    }
  }
  else
  {
    sleep(20);
    printf("我是父进程，我的pid是的%d,我已经正在准备子进程了\n",getpid());
    pid_t ret = wait(NULL);
    if(ret < 0 )
    {
      printf("等待失败\n");
    }
    else
    {
      printf("等待成功\n");   
    }
    sleep(10);

  }
}
