#include<stdio.h>

#define PRIT_I 0x1
#define PRIT_L 0x2
#define PRIT_V 0x4
#define PRIT_U 0x8

void fuc(int flags)
{
  if(flags & PRIT_I)
    printf("I ");
  if(flags & PRIT_L)
    printf("L ");
  if(flags & PRIT_V)
    printf("V ");
  if(flags & PRIT_U)
    printf("U");
  
}

int main()
{
  fuc(PRIT_I);
  fuc(PRIT_L);
  fuc(PRIT_V);
  fuc(PRIT_U);
  printf("\n");
  fuc(PRIT_I);
  printf("\n");
  fuc(PRIT_I | PRIT_L);
  printf("\n");
  fuc(PRIT_I | PRIT_L | PRIT_V);
  printf("\n");
  fuc(PRIT_I | PRIT_L | PRIT_V | PRIT_U);
  printf("\n");

  return 0;
}

