#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/wait.h>



int main()
{
  pid_t pid = fork();
  if(pid == 0)
  {
    int a = 5;
    while(1)
    {
      printf("我是子进程，我的pid是%d\n",getpid());
      sleep(5);
      break;
    }
    exit(139);
    
  }
  else
  {
    int status = 0;
    printf("我是父进程，我的pid是%d，我准备等待子进程\n",getpid());
    pid_t wt = waitpid(pid,&status,0);
    if(WIFEXITED(status))
    {
      printf("wait succes，等待的进程是%d，该进程的退出码是%d\n",wt,WEXITSTATUS(status));
    }
  }



}
