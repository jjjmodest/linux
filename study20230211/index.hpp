#pragma once
#include <iostream>
#include <string>
#include <vector>
using namespace std;

namespace ns_indx
{
    typedef struct DocInfo
    {
        string title;   // 文档的标题
        string content; // 文档去标签化之后的内容
        string url;     // 该文档在官网中的url
        int doc_id;     // 文档的id
    } DocInfo_t;

    struct InvertedElem
    {
        // 倒排索引
        int doc_id;  // 文档的id
        string word; // 关键词
        int weight;  // 权重
    };

    class Index
    {
    private:
        // 正排索引的数据结构用数组，数组下标就是文档id
        vector<DocInfo_t> forward_index; // 正排索引
    };
}