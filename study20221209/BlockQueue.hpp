#pragma once

#include <iostream>
#include <queue>
#include <unistd.h>
#include <pthread.h>
#include <cstdlib>
#include <ctime>
using namespace std;

const uint32_t capacity = 5;

template <class T>
class Blockqueue
{
public:
    Blockqueue(const uint32_t cap = capacity)
        : _capacity(cap)
    {
        pthread_mutex_init(&_mutex, nullptr);
        pthread_cond_init(&_concond, nullptr);
        pthread_cond_init(&_procond, nullptr);
    }
    ~Blockqueue()
    {
        pthread_mutex_destroy(&_mutex);
        pthread_cond_destroy(&_concond);
        pthread_cond_destroy(&_procond);
    }

    // 生产
    void produce(const T &in)
    {
        // 加锁
        lock();
        // 判断是否有条件进行生产
        // 不能生产
        if (isFull())
        {
            // 等待消费者将数据消费出来
            ProWaitCon();
        }
        // 能生产
        push(in);
        // 解锁
        unlock();
        // 唤醒消费者
        wakeCon();
    }

    // 消费
    T consume()
    {
        // 加锁
        lock();
        // 判断是否有条件进行消费
        // 不能消费
        if (isEmpty())
        {
            // 等待生产者将数据生产出来
            ConWaitPro();
        }
        // 能消费
        T out = pop();
        // 解锁
        unlock();
        // 唤醒生产者
        wakePro();
        return out;
    }

private:
    void ProWaitCon()
    {
        // 线程在阻塞时，mutex会自动释放
        pthread_cond_wait(&_procond, &_mutex);
        // 能自动释放，当被唤醒时也会自动上锁
    }
    void ConWaitPro()
    {
        pthread_cond_wait(&_concond, &_mutex);
    }
    void wakePro()
    {
        pthread_cond_signal(&_procond);
    }
    void wakeCon()
    {
        pthread_cond_signal(&_concond);
    }
    void lock()
    {
        pthread_mutex_lock(&_mutex);
    }
    void unlock()
    {
        pthread_mutex_unlock(&_mutex);
    }
    bool isEmpty()
    {
        return _q.empty();
    }
    bool isFull()
    {
        return _capacity == _q.size();
    }
    void push(const T& in)
    {
        _q.push(in);
    }
    T pop()
    {
        T temp = _q.front();
        _q.pop();
        return temp;
    }

private:
    // size
    uint32_t _capacity;
    // 队列
    queue<T> _q;
    // mutex
    pthread_mutex_t _mutex;
    // 让消费者等待的条件变量
    pthread_cond_t _concond;
    // 让生产者等待的条件变量
    pthread_cond_t _procond;
};
