#include "BlockQueue.hpp"

// int main()
// {
//     Blockqueue<int> bq;
//     bq.produce(100);
//     int out = bq.consume();
//     cout << out << endl;
//     bq.produce(1100);
//     out = bq.consume();
//     cout << out << endl;
//     bq.produce(1010);
//     out = bq.consume();
//     cout << out << endl;
//     bq.produce(1001);
//     out = bq.consume();
//     cout << out << endl;
//     bq.produce(10011);
//     out = bq.consume();
//     cout << out << endl;
// }
// void *concumer(void *queue)
// {
//     Blockqueue<int> *bqp = static_cast<Blockqueue<int> *>(queue);
//     while (1)
//     {
//         int out = bqp->consume();
//         cout << "consume data success,data is : " << out << "." << endl;
//         sleep(1);
//     }
// }

// void *producer(void *queue)
// {
//     Blockqueue<int> *bqp = static_cast<Blockqueue<int> *>(queue);
//     while (1)
//     {
//         // 制作数据
//         int data = rand() % 100;
//         // 生产数据
//         bqp->produce(data);
//         cout << "produce data success,data is : " << data << endl;
//     }
// }

// int main()
// { // 种随机数种子
//     srand((unsigned int)time(nullptr) ^ getpid());

//     Blockqueue<int> bq;
//     pthread_t con, prd;

//     pthread_create(&con, nullptr, concumer, &bq);
//     pthread_create(&prd, nullptr, producer, &bq);

//     pthread_join(con, nullptr);
//     pthread_join(prd, nullptr);

//     return 0;
// }

// //条件变量
// pthread_cond_t cond;
// //mutex
// pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
// //线程运行的函数
// void *fuc(void *argc)
// {
//     while (1)
//     {
//         pthread_mutex_lock(&mutex);
//         //等待唤醒
//         pthread_cond_wait(&cond, &mutex);
//         cout << "my thread id : " << pthread_self() << endl;
//         pthread_mutex_unlock(&mutex);
//         break;
//     }
// }

// int main()
// {
//     pthread_t t1, t2, t3;

//     pthread_create(&t1, nullptr, fuc, NULL);
//     pthread_create(&t2, nullptr, fuc, NULL);
//     pthread_create(&t3, nullptr, fuc, NULL);

//     while (1)
//     {
//         char a;
//         cout << "请输入n/q : ";
//         cin >> a;
//         //n为next q为quit
//         if (a == 'n')
//         {
//             //通过条件变量唤醒一个线程
//             pthread_cond_signal(&cond);
//         }
//         else
//         {
//             break;
//         }

//         sleep(0.5);
//     }
//     cout<<"ready to cancel"<<endl;
//     pthread_cancel(t1);
//     pthread_cancel(t2);
//     pthread_cancel(t3);
//     cout<<"cancel success"<<endl;

//     pthread_cond_broadcast(&cond);
//     cout<<"wake all success"<<endl;

//     pthread_join(t1, NULL);
//     pthread_join(t2, NULL);
//     pthread_join(t3, NULL);
//     cout<<"join success"<<endl;
 
//     pthread_cond_destroy(&cond);
//     pthread_mutex_destroy(&mutex);
//     return 0;
// }

