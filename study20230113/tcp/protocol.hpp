#pragma once
#include "util.hpp"

// 定制请求
class Request
{
public:
    Request()
    {
    }
    ~Request()
    {
    }
    // 序列化
    void serialize(string *out)
    {
    }
    // 反序列化
    bool Deserialize(const string &in)
    {
    }

    // encode 为整个序列化之后的字符串添加长度
    string encode(const string &in, uint32_t len)
    {
    }

    // decode 为整个序列化之后的字符串提取长度
    string decode(const string &in, uint32_t *len)
    {
    }

    void getnum(int *x, int *y, char *op)
    {
    }

public:
    int _x;
    char opr;
    int _y;
};

// 定制响应
class Responce
{
public:
    Responce():_resultcode(0),_result(0)
    {
    }
    ~Responce()
    {
    }
    // 序列化
    void serialize(string *out)
    {
    }
    // 反序列化
    bool Deserialize(const string &in)
    {
    }

    // encode 为整个序列化之后的字符串添加长度
    string encode(const string &in, uint32_t len)
    {
    }

    // decode 为整个序列化之后的字符串提取长度
    string decode(const string &in, uint32_t *len)
    {
    }

public:
    int _resultcode;
    int _result;
};