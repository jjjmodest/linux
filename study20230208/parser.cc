#include <iostream>
#include <string>
#include <vector>
#include <boost/filesystem.hpp>
using namespace boost::filesystem;
using namespace std;

// 存放所有html的文件的路径
const string src_path = "data/input/";
// 存放去除标签化的内容的路径
const string raw_path = "data/raw_html/raw.txt";

typedef struct DocInfo
{
    string title;   // 文档的标题
    string content; // 文档内容
    string url;     // 该文档在官网中的url
} DocInfo_t;

// const & : 输入
//* : 输出
//& ：输入输出

bool EnumFile(const string &src_path, vector<string> *files_list)
{
    path root_path(src_path); // path是类型 root_path通过src_path初始化

    if (!exists(root_path)) // 判断是否root_path存在
    {
        cerr << src_path << "not exists" << endl;
        return false;
    }

    // 定义一个空的迭代器，用来进行判断递归结束
    recursive_directory_iterator end;
    for (recursive_directory_iterator iter(root_path); iter != end; iter++)
    {
        // 从root_path开始递归遍历
        if (!is_regular_file(*iter))
        {
            // 不是常规文件就continue
            continue;
        }

        if (iter->path().extension() != ".html")
        {
            // 后缀不是.html继续continue path()获取路径 extension()获取后缀
            continue;
        }

        //cout << "debug: " << iter->path().string() << endl;
        // 走到这里说明是以.html结尾的网页文件
        files_list->push_back(iter->path().string());
        // 可以将我们对象所对应的路径以字符串的形式传入
        // 将所有的.html文件的路径放入files_list中以便后面的文本分析。
    }
    return true;
}

bool ParseHtml(const vector<string> &files_list, vector<DocInfo_t> *results)
{

    return true;
}

bool SaveHtml(const vector<DocInfo_t> &results, const string &raw_path)
{

    return true;
}

int main()
{
    vector<string> files_list;
    // 1. 通过该函数递归式的将每个html文件带路径，保存到files_list中。
    if (!EnumFile(src_path, &files_list))
    {
        cerr << "EnumFile error" << endl;
        return 1;
    }

    // 2. 通过files_list中的内容进行解析
    vector<DocInfo_t> results;
    if (!ParseHtml(files_list, &results))
    {
        cerr << "ParseHtml error" << endl;
        return 2;
    }

    // 3. 把解析完毕的各个文件内容写入到raw_path中，按照"\3"作为每个文档内容之间的分隔符
    if (!SaveHtml(results, raw_path))
    {
        cerr << "SaveHtml error" << endl;
        return 3;
    }

    return 0;
}
