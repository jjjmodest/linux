#include "searcher.hpp"

const string input = "data/raw_html/raw.txt";
int main()
{
    ns_searcher::Searchear *searcher = new ns_searcher::Searchear();
    searcher->InitSercher(input);

    string query;
    string json_string;
    while (1)
    {
        cout << "Please entry your query#  " << endl;
        cin >> query;
        searcher->Searcher(query, &json_string);

        cout << json_string << endl;
    }
    return 0;
}