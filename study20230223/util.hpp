#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <mutex>
#include <unordered_map>
#include <boost/algorithm/string.hpp>
#include "jiebacpp/cppjieba/Jieba.hpp"
using namespace std;

namespace ns_util
{
    class FileUtil
    {
    public:
        static bool ReadFile(const string &file_path, string *out)
        {
            ifstream in(file_path.c_str(), ios::in); // 以读的方式打开文件
            if (!in.is_open())
            {
                cerr << "open file: " << file_path << "error" << endl;
                return false;
            }

            string line;
            while (getline(in, line)) // 按行读取 读取到line中
            {

                // 明明getline的返回值是&，while里要判断的是bool类型的值
                // 因为返回的对象重载了强制类型转换，对象会被转换为bool类型
                *out += line;
            }

            in.close();
            return true;
        }
    };
    class IndexUtil
    {
    public:
        static void CutString(const string &target, vector<string> *out, const string &sep)
        {
            // 关于字符串切分使用string中的find之后再有substr截取也行，我们这次使用现成的boost库中的split 关于split的用法很简单
            // 第一个参数为输出的位置 第二个参数为要裁剪的目标string 第三个参数为作为分割字符串的分隔符 第四个参数为是否要将重复的分割符合成一个
            boost::split(*out, target, boost::is_any_of(sep), boost::token_compress_on);
        }
    };
    const char *const DICT_PATH = "./dict/jieba.dict.utf8";
    const char *const HMM_PATH = "./dict/hmm_model.utf8";
    const char *const USER_DICT_PATH = "./dict/user.dict.utf8";
    const char *const IDF_PATH = "./dict/idf.utf8";
    const char *const STOP_WORD_PATH = "./dict/stop_words.utf8";
    class JiebaUtil
    {
    private:
        static cppjieba::Jieba jieba;
    public:
        static void CutString(const string &src, vector<string> *out)
        {
            jieba.CutForSearch(src, *out);
        }
    };
    cppjieba::Jieba JiebaUtil::jieba(DICT_PATH, HMM_PATH, USER_DICT_PATH, IDF_PATH, STOP_WORD_PATH);
}