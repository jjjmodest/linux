#include "comm.h"
// read

using namespace std;

int main()
{
    umask(0);
    if (mkfifo(PATH, 0600) != 0)
    {
        cerr << "mkfifo error" << endl;
        return 1;
    }

    int pipefd = open(PATH, O_RDONLY);
    if (pipefd < 0)
    {
        cout << "open error" << endl;
    }

    char arr[1024];
    while (1)
    {
        ssize_t sz = read(pipefd, arr, sizeof(arr) - 1);
        if (sz > 0)
        {
            arr[sz] = '\0';
            cout << "客户端:" << arr<<endl ;
        }
        else if(sz == 0)
        {
            cout<<"exit"<<endl;
            break;
        }
        else 
        {
            cout<<"error is:"<<strerror(errno)<<endl;
            break;
        }

    }

    close(pipefd);
    unlink(PATH);
    return 0;
}