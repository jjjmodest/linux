#include "comm.h"

using namespace std;

int main()
{
    int fd = open(PATH,O_WRONLY);
    if(fd < 0)
    {
        cerr << "open error" << strerror(errno) <<endl;
    }

#define NUM 1024
    char line[NUM];
    while(true)
    {
        printf("please stdin: ");
        fflush(stdout);
        memset(line,0,sizeof(line));
        if(fgets(line,sizeof(line),stdin) != nullptr)
        {
            line[strlen(line)-1] = '\0';
            write(fd,line,strlen(line));
        }
        else
        {
            break;
        }
    }

    close(fd);
    cout<<"客户端退出"<<endl;
    return 0;
}