#include <iostream>

#include <sys/types.h>
#include <sys/socket.h>
#include <string>
#include <unordered_map>
#include <ctype.h>
#include <cstring>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <pthread.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <queue>
#include <cstdlib>
#include <memory>
#include <ctime>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "log.hpp"
using namespace std;

#define SOCKET_ERR 1
#define BIND_ERR 2
#define LISTEN_ERR 3
#define USAGE_ERR 4
#define CONN_ERR 5

#define BUFFER_SIZE 1024
