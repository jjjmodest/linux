#include "util.hpp"
#include <string>
#include <cstring>
#include <vector>
#include <functional>
using namespace std;
using namespace Util;

using func_t = function<void()>;

void Show()
{
    cout<<"我们正在处理其他事情"<<endl;
}

int main()
{
    vector<func_t> fucs;
    fucs.push_back(Show);
    char buffer[1024];
    while (1)
    {
        SetNonBlock(0);
        // string s;
        // cin >> s;
        buffer[0] = 0;
        // memset(buffer,0,1024);
        int n = scanf("%s", buffer);
        if (n == -1)
        {
            cout << errno << ' ' << strerror(errno) << endl;
            for(auto &f : fucs)
            {
                f();
            }
        }
        else
        {
            cout << "$$ " << buffer << endl;
        }
        sleep(1);
    }
}
