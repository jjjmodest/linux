const int gthreadnum = 8;

template <class T>
class Threadpool
{
public:
    Threadpool(int num = gthreadnum) : _isStart(false), _threadnum(num)
    {
        assert(_threadnum > 0);
        pthread_mutex_init(&_mutex, nullptr);
        pthread_cond_init(&_cond, nullptr);
    }
    ~Threadpool()
    {
        pthread_mutex_destroy(&_mutex);
        pthread_cond_destroy(&_cond);
    }
    static void *fuc(void *argc)
    {
        pthread_detach(pthread_self());
        Threadpool<T> *tp = static_cast<Threadpool<T> *>(argc);
        while (1)
        {
            tp->lockqueue();
            while (tp->isempty())
            {
                tp->waittask();
            }
            T t = tp->pop();
            tp->unlockqueue();
            int result = t.calculate();
            int one, two;
            char opr;
            t.getelem(&one, &two, &opr);
            cout << "Time : " << (unsigned long)time(nullptr) << ' ' << "thread : " << pthread_self()
                 << ' ' << "do task success,data is : " << one << ' ' << opr << ' ' << two << " = " << result << endl;
        }
    }
    void start()
    {
        assert(!_isStart);
        for (int i = 0; i < _threadnum; i++)
        {
            pthread_t th1;
            pthread_create(&th1, nullptr, fuc, this);
        }
        _isStart = true;
    }
    void push(const T &in)
    {
        lockqueue();
        _taskqueue.push(in);
        choicethread();
        unlockqueue();
    }

private:
    void lockqueue()
    {
        pthread_mutex_lock(&_mutex);
    }
    void unlockqueue()
    {
        pthread_mutex_unlock(&_mutex);
    }
    bool isempty()
    {
        return _taskqueue.size();
    }
    void waittask()
    {
        pthread_cond_wait(&_cond, &_mutex);
    }
    void choicethread()
    {
        pthread_cond_signal(&_cond);
    }
    T pop()
    {
        T temp = _taskqueue.front();
        _taskqueue.pop();
        return temp;
    }

private:
    bool _isStart;
    uint32_t _threadnum;
    queue<T> _taskqueue;
    // 让线程互斥的获取任务队列的任务
    pthread_mutex_t _mutex;
    pthread_cond_t _cond;
};