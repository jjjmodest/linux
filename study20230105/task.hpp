#include "util.hpp"

class Task
{
public:
    // 等价于 typedef function<void (int,string,uint16_t)> callback_t
    using callback_t = function<void(int, string, uint16_t)>;

private:
    int _sock;
    uint16_t _port;
    string _ip;
    callback_t _fuc;

public:
    Task()
        : _sock(-1), _port(-1)
    {
    }
    Task(int sock, uint16_t port, const string ip, callback_t fuc)
        : _sock(sock), _port(port), _ip(ip), _fuc(fuc)
    {
    }
    ~Task()
    {
    }
};