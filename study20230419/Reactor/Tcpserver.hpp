#pragma once

#include <iostream>
#include <string>
#include <unordered_map>
#include <cstdlib>
#include <sys/epoll.h>
#include <cassert>
#include <functional>
#include "Sock.hpp"
#include "Log.hpp"
#include "Epoller.hpp"
using namespace std;

class Connection;
class Tcpserver;
using fuc_t = function<int(Connection *)>;

class Connection
{

public:
    // 文件描述符
    int _sock;

    Tcpserver *_ptr;
    //  自己的接受和发送缓冲区
    string _inbuff;
    string _outbuff;

    // 回调函数
    fuc_t _readfuc;
    fuc_t _writefuc;
    fuc_t _exceptfuc;

public:
    Connection(int sock, Tcpserver *ptr) : _sock(sock), _ptr(ptr)
    {
    }
    ~Connection()
    {
    }
    void SetReadfuc(fuc_t fuc)
    {
        _readfuc = fuc;
    }
    void SetWritefuc(fuc_t fuc)
    {
        _writefuc = fuc;
    }
    void SetExceptfuc(fuc_t fuc)
    {
        _exceptfuc = fuc;
    }
};

class Tcpserver
{
public:
    Tcpserver(int port)
    {

        // 网络
        _listensock = Sock::Socket();
        Sock::Bind(_listensock, port);
        Sock::Listen(_listensock);

        // epoll
        _epfd = Epoller::CreateEpoll();

        // add事件
        Epoller::Addevent(_epfd, _listensock, EPOLLIN | EPOLLET);

        // 将listensock匹配的connection方法添加到unordered_map中
        auto iter = new Connection(_listensock, this);
        iter->SetReadfuc(std::bind(&Tcpserver::Accepter, this, std::placeholders::_1));
        _conn.insert({_listensock, iter});

        // 初始化就绪队列
        _revs = new struct epoll_event[_revs_num];
    }
    int Accepter(Connection *conn)
    {
        string clientip;
        uint16_t clientport;
        int sockfd = Sock::Accept(conn->_sock, &clientip, &clientport);
        if (sockfd < 0)
        {
            logMessage(FATAL, "accept error");
            return -1;
        }
        logMessage(DEBUG, "Get a new connect : %d", sockfd);
        AddConn(sockfd, EPOLLIN | EPOLLET);
        return 0;
    }
    bool SockinConn(int sock)
    {
        auto iter = _conn.find(sock);
        if (iter == _conn.end())
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    void AddConn(int sock, uint32_t event)
    {
        // 将文件描述符加入epoll模型中
        Epoller::Addevent(_epfd, sock, event);
        // 将文件描述符匹配的connection，也加入map中
        _conn.insert({sock, new Connection(sock, this)});
        logMessage(DEBUG, "将文件描述符匹配的connection加入map成功");

    }

    void Dispatcher()
    {
        // 获取就绪事件
        int n = Epoller::GetReadyFd(_epfd, _revs, _revs_num);
        // logMessage(DEBUG, "GetReadyFd,epoll_wait");
        // 事件派发
        for (int i = 0; i < n; i++)
        {
            int sock = _revs[i].data.fd;
            uint32_t revent = _revs[i].events;
            if (EPOLLIN & revent)
            {
                // 先判空
                if (SockinConn(sock) && _conn[sock]->_readfuc)
                {
                    // 该文件描述符对应的读方法
                    _conn[sock]->_readfuc(_conn[sock]);
                }
            }
            if (SockinConn(sock) && EPOLLOUT & revent)
            {
                // 先判空
                if (_conn[sock]->_writefuc)
                {
                    // 该文件描述符对应的写方法
                    _conn[sock]->_writefuc(_conn[sock]);
                }
            }
        }
    }

    void Run()
    {
        while (1)
        {
            Dispatcher();
        }
    }
    ~Tcpserver()
    {
        if (_listensock != -1)
            close(_listensock);
        if (_epfd != -1)
            close(_epfd);
        delete[] _revs;
    }

private:
    // 1.网络sock
    int _listensock;
    // 2.epoll
    int _epfd;
    // 3.将epoll与上层代码结合
    unordered_map<int, Connection *> _conn;
    // 4.就绪事件列表
    struct epoll_event *_revs;
    // 5.就绪事件列表大小
    static const int _revs_num = 64;
};