#include "searcher.hpp"
#include <cstdio>

const string input = "data/raw_html/raw.txt";
int main()
{
    ns_searcher::Searchear *searcher = new ns_searcher::Searchear();
    searcher->InitSercher(input);

    string query;
    string json_string;
    char buff[1024];
    while (1)
    {
        cout << "Please entry your query#  " << endl;
        fgets(buff,sizeof(buff)-1,stdin);
        searcher->Searcher(query, &json_string);
        buff[strlen(buff)-1] = 0;
        query = buff;
        cout << json_string << endl;
    }
    return 0;
}