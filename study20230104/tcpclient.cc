#include "util.hpp"

volatile bool quit = false;

static void Usage(const string proc)
{
    cout << "Usage:\n\t"
         << "server IP ,server port" << endl;
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        Usage(argv[0]);
        exit(1);
    }
    // 获取服务端
    string serverip = argv[1];
    uint16_t serverport = atoi(argv[2]);

    // 1 创建socket
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0)
    {
        cerr << "socket error : " << strerror(errno) << endl;
        exit(1);
    }

    // 2 connect
    // 2.1 填充
    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(serverip.c_str());
    server.sin_port = htons(serverport);

    // 3 连接
    if (connect(sock, (const struct sockaddr *)&server, sizeof(server)) != 0)
    {
        cerr << "connect error : " << strerror(errno) << endl;
        ;
        exit(2);
    }
    cout << "connect sueccess : " << sock << endl;

    string message;
    while (!quit)
    {
        message.clear();
        cout << "Plz entry : ";
        getline(cin, message);
        if (strcasecmp(message.c_str(), "quit") == 0)
        {
            quit = true;
        }

        ssize_t s = write(sock, message.c_str(), message.size());
        if (s > 0)
        {
            message.resize(1024);
            ssize_t s = read(sock, (char *)message.c_str(), 1024);
            if (s > 0)
            {
                message[s] = '\0';
            }
            cout << "server transfer"
                 << ">>> " << message << endl;
        }
        else if (s <= 0)
        {
            break;
        }
    }
    close(sock);
    return 0;
}