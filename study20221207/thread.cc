#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <functional>
#include <vector>
using namespace std;

//条件变量
pthread_cond_t cond;
//mutex
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
//vector fuc 集合
vector<function<void()>> fucs;

void Show()
{
    cout<<"Hello show!"<<endl;
}

void Print()
{
    cout<<"Hello print!"<<endl;
}



//线程运行的函数
void *fuc(void *argc)
{
    while (1)
    {
        pthread_cond_wait(&cond, &mutex);
        for(auto &ch : fucs)
        {
            ch();
        }
        cout << "my thread id : " << pthread_self() << endl;
    }
}


int main()
{
    fucs.push_back(Show);
    fucs.push_back(Print);
    fucs.push_back([](){
        cout<<"Im god"<<endl;
    });
    pthread_t t1, t2, t3;

    pthread_create(&t1, nullptr, fuc, NULL);
    pthread_create(&t2, nullptr, fuc, NULL);
    pthread_create(&t3, nullptr, fuc, NULL);

    while (1)
    {
        char a;
        cout << "请输入n/q : ";
        cin >> a;
        //n为next q为quit
        if (a == 'n')
        {
            //通过条件变量唤醒一个线程
            pthread_cond_signal(&cond);
        }
        else
        {
            break;
        }

        sleep(0.5);
    }

    pthread_cancel(t1);
    pthread_cancel(t2);
    pthread_cancel(t3);

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    pthread_join(t3, NULL);
    pthread_cond_destroy(&cond);
    return 0;
}
