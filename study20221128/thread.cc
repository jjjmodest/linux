#include <iostream>
#include <pthread.h>
#include <unistd.h>
using namespace std;

int count = 0;

pthread_mutex_t mutex;

void *fuc(void *argc)
{

    for (int i = 0; i < *(int *)argc; i++)
    {
        pthread_mutex_lock(&mutex);
        count++;
        cout<<"我的线程ID是: "<<pthread_self()<<endl;
        pthread_mutex_unlock(&mutex);
    }

    // sleep(2);
    // cout << "My tid : " << pthread_self() << endl;
    // exit(2);
}

// void *fuc1(void *argc)
// {
//     for (int i = 0; i < *(int *)argc; i++)
//     {
//         count++;
//     }

//     // sleep(2);
//     // cout << "My tid : " << pthread_self() << endl;
//     // exit(2);
// }

// int main()
// {
//     int num = 100000;

//     pthread_t tid;
//     // pthread_t tid1;
//     pthread_create(&tid, NULL, fuc, (void *)&num);
//     // pthread_create(&tid1, NULL, fuc1, (void *)&num);
//     // while (1)
//     // {
//     //     sleep(1);
//     //     cout << "I'm come back" << endl;
//     // }
//     pthread_join(tid, NULL);
//     // pthread_join(tid1, NULL);
//     cout << "The count is : " << count << endl;
//     return 0;
// }
int main()
{
    int num = 100000;
    pthread_mutex_init(&mutex, nullptr);

    pthread_t tid;
    pthread_t tid1;
    
    pthread_create(&tid, NULL, fuc, (void *)&num);
    pthread_create(&tid1, NULL, fuc, (void *)&num);
    
    pthread_join(tid, NULL);
    pthread_join(tid1, NULL);

    pthread_mutex_destroy(&mutex);
    cout << "The count is : " << count << endl;
    return 0;
}