#include <iostream>
#include <string>
#include <vector>
using namespace std;

// 存放所有html的文件的路径
const string src_path = "data/input/";
// 存放去除标签化的内容的路径
const string raw_path = "data/raw_html/raw.txt";

typedef struct DocInfo
{
    string title;   // 文档的标题
    string content; // 文档内容
    string url;     // 该文档在官网中的url
} DocInfo_t;

int main()
{
    vector<string> files_list;
    // 1. 通过该函数递归式的将每个html文件带路径，保存到files_list中。
    if (!EnumFile(src_path, &files_list))
    {
        cerr << "EnumFile error" << endl;
        return 1;
    }

    // 2. 通过files_list中的内容进行解析
    vector<DocInfo_t> results;
    if (!ParseHtml(files_list, results))
    {
        cerr << "ParseHtml error" << endl;
        return 2;
    }

    // 3. 把解析完毕的各个文件内容写入到raw_path中，按照"\3"作为每个文档内容之间的分隔符
    if(!SaveHtml(results,&raw_path))
    {
        cerr << "SaveHtml error"<<endl;
    }

}
