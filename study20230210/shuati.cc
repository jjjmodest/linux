#include <iostream>
#include <stack>
using namespace std;

class Solution {
public:
    string decodeString(string s) 
    {
        stack<int> stint;
        stack<string> ststr;
        string str;
        for(int i =0;i<s.size();i++)
        {
            if(isdigit(s[i]))
            {
                int n = s[i] -'0';
                while(isdigit(s[++i]))
                {
                    n = n*10 + s[i]-'0';
                }
                stint.push(n);
                --i;
            }
            else if(s[i] =='[')
            {
                ststr.push(str);
                str="";
            }
            else if(s[i] =']')
            {
                string tmp;
                for(int num =0;num<stint.top();num++)
                {
                    tmp += str;
                }

                str = tmp;
                str = ststr.top() + str;
                ststr.pop();
                stint.pop();
            }
            else
            {
                str += s[i];
            }
            
        }
        return str;
    }
};