#include <iostream>
#include <string>
#include <fstream>
using namespace std;

namespace ns_util
{
    class FileUtil
    {
    public:
        static bool ReadFile(const string &file_path, string *out)
        {
            ifstream in(file_path,ios::in); // 以读的方式打开文件
            if(!in.is_open())
            {
                cerr<<"open file: "<<file_path<<"error"<<endl;
                return false;
            }

            string line;
            while(getline(in,line)) // 按行读取 读取到line中
            {

                // 明明getline的返回值是&，while里要判断的是bool类型的值
                // 因为返回的对象重载了强制类型转换，对象会被转换为bool类型
                *out += line;
            }

            in.close();
            return true;
        }
    };
}