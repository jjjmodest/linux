
#include <algorithm>
#include <unordered_map>
class Solution {
public:
    /**
     * 
     * @param arr int整型vector the array
     * @return int整型
     */
    int maxLength(vector<int>& arr) 
    {
        if(arr.size() == 1)
        {
            return 1;
        }
        unordered_map<int, int> map;
        int ret = 0;
        int left = 0;
        int right = 0;
        int count = 0;
        while(left != arr.size())
        {
            if(map[arr[left]] > 0)
            {
                map.erase(arr[right]);
                right++;
            }
            else 
            {
                map[arr[left]]++;
                left++;
            }
            ret = ret > map.size() ? ret : map.size();
        }
        return ret;
    }
};