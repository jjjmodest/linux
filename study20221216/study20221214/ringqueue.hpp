#include <iostream>
#include <vector>
#include <ctime>
#include <unistd.h>
#include <semaphore.h>
using namespace std;

const int size = 8;

template <class T>
class Ringqueue
{
public:
    Ringqueue(int cap = size)
        : _ringqueue(cap), _proindex(0), _conindex(0)
    {
        // 初始化信号量
        sem_init(&_dataspace, 0, _ringqueue.size());
        sem_init(&_datanum, 0, 0);
    }

    void produce(const T &in)
    {
        // 等待信号量，如果有就会使该信号量减减继续如果没有就阻塞
        sem_wait(&_dataspace);
        _ringqueue[_proindex] = in;

        // 增加信号量
        sem_post(&_datanum);

        _proindex++;
        _proindex %= _ringqueue.size();
    }
    T consume()
    {
        sem_wait(&_datanum);
        T out = _ringqueue[_conindex];

        sem_post(&_dataspace);

        _conindex++;
        _conindex %= _ringqueue.size();

        return out;
    }
    ~Ringqueue()
    {
        // 销毁信号量
        sem_destroy(&_dataspace);
        sem_destroy(&_datanum);
    }

private:
    // queue
    vector<T> _ringqueue;
    // 信号量
    // 空间大小
    sem_t _dataspace;
    // 数据个数
    sem_t _datanum;
    // 生产者生产的下标
    uint32_t _proindex;
    // 消费者消费的下标
    uint32_t _conindex;
};
