#include "ringqueue.hpp"

void *productor(void *args)
{
    Ringqueue<int> *rqp = static_cast<Ringqueue<int> *>(args);
    while (1)
    {
        sleep(1);
        int data = rand() % 199;
        rqp->produce(data);
        cout << "Time : " << (unsigned long)time(nullptr) << ' ' << "thread : " << pthread_self()
             << ' ' << "produce data success,data is : " << data << endl;
    }
}

void *concumer(void *args)
{
    Ringqueue<int> *rqp = static_cast<Ringqueue<int> *>(args);
    while (1)
    {
        int out = rqp->consume();
        cout << "Time : " << (unsigned long)time(nullptr) << ' ' << "thread : " << pthread_self()
             << ' ' << "consume data success,data is : " << out << endl;
    }
}

int main()
{
    srand((unsigned long)time(nullptr) ^ getpid());
    pthread_t con;
    pthread_t pro;

    Ringqueue<int> rq;
    pthread_create(&con, nullptr, concumer, &rq);
    pthread_create(&pro, nullptr, productor, &rq);

    pthread_join(con, nullptr);
    pthread_join(pro, nullptr);
    return 0;
}