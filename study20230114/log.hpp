#pragma once
#include <cstdio>
#include <cstdarg>
#include <cassert>
#include <stdlib.h>
#include <time.h>

#define DEBUG 0
#define NOTICE 1
#define WARNING 2
#define FATAL 3

#define LOGFIFE "tcpserver.log"

const char *log_level[] = {"DEBUG", "NOTICE", "WARNING", "FATAL"};

class Log
{
public:
    Log() : _logfd(-1)
    {
    }
    void startlog()
    {
        umask(0);
        _logfd = open(LOGFIFE, O_WRONLY | O_CREAT | O_APPEND, 0666);
        assert(_logfd != -1);
        dup2(_logfd, 1);
        dup2(_logfd, 2);
    }
    ~Log()
    {
        if (_logfd != -1)
        {
            fsync(_logfd);
            close(_logfd);
        }
    }

public:
    int _logfd;
};

void logMessage(int level, const char *format, ...)
{
    assert(level >= DEBUG);
    assert(level <= FATAL);
    char logInfor[1024];
    char *name = getenv("USER");
    va_list ap;
    va_start(ap, format);

    vsnprintf(logInfor, sizeof(logInfor) - 1, format, ap);

    va_end(ap);

    // umask(0);
    // int fd = open(LOGFIFE, O_WRONLY | O_CREAT | O_APPEND,0666);

    FILE *out = (level == FATAL) ? stderr : stdout;
    fprintf(out, "%s | %u | %s | %s\n",
            log_level[level],
            (unsigned int)time(nullptr),
            name == nullptr ? "Unkown" : name,
            logInfor);

    // 将C缓冲区中的内容刷新到os中
    fflush(out);
    // 将os中的数据尽快刷到磁盘中
    fsync(fileno(out));
}