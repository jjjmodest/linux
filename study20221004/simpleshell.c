#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 1024
#define NUM 128

#define DIV " "

char command_line[SIZE];
char *command_args[NUM];

char env_storge[NUM];

int main()
{
  while(1)
  {
    //1.显示窗口
    printf("[尊敬的主人]$");
    fflush(stdout);
    
    //2.获取用户输入
    memset(command_line,'\0',sizeof(command_line)*sizeof(char));
    fgets(command_line,SIZE,stdin);
    command_line[strlen(command_line)-1] = '\0';
    
    //3.字符串分割
    command_args[0] = strtok(command_line,DIV);
    int a = 1;
    while(command_args[a++] = strtok(NULL,DIV));

    if(strcmp(command_args[0],"cd") == 0 && command_args[1] != NULL)
    {
      chdir(command_args[1]);
      continue; 
    }

    if(strcmp(command_args[0],"export") == 0 && command_args[1] != NULL)
    {
      strcpy(env_storge,command_args[1]);
      putenv(env_storge);
      continue;
    }
  
    //4.创建子进程，执行
    pid_t id = fork();
    if(id == 0)
    {
      //child
      //5.程序替换
      execvp(command_args[0],command_args);
      
      exit(1);
    }
    int status = 0;
    pid_t ret = waitpid(id,&status,0);
    if(ret > 0)
    {
      printf("等待子进程成功：退出码：%d 退出信号：%d\n", (status>>8)&0xFF , status&0x7F);
    }
  }
  return 0;
}
