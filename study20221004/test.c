#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<stdlib.h>


int main()
{
  //printf("我是一个进程，将要执行目标进程\n");
  //execl("/usr/bin/ls","ls","-l","-a",NULL);
  //printf("我是目标进程，我执行完毕\n");
 
  //printf("我是一个进程，将要执行目标进程\n");
  //环境变量的声明
  extern char**environ; 
  pid_t id = fork();
  //char * const argv[] = {(char*)"ls",(char*)"-l",(char*)"-a",NULL};
  if(id == 0)
  {

   //char * const env_[] = {(char*)"MYPATH=One day you can!",NULL};
    printf("我是子进程我要进行一个新的进程!\n ");
    execle("/home/Yewei/Code/study20221004/circulate","circulate",NULL,environ);
  
   // execl("/home/Yewei/Code/study20221004/circulate","circulate",NULL); 
   // execlp("ls","ls","-a","-l",NULL);
   //execv("/usr/bin/ls",argv);
   // execl("/usr/bin/ls","ls" ,"-l","-a",NULL);
    exit(12);
  }

  //父进程
  int status = 0;
  printf("我是父进程，我正在等待子进程！");
  pid_t ret = waitpid(id,&status,0);
  if(ret == id)
  {
    sleep(2);
    printf("等待成功,退出码是%d\n",WEXITSTATUS(status));
  }  
  return 0;
}
