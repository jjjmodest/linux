#pragma once 

#include <iostream>
#include <ctime>


std::ostream& output()
{
    std::cout<<"For debug:"<<std::endl;
    std::cout<<"Time: "<< (uint64_t)time(nullptr)<<std::endl;
    return std::cout; 
}