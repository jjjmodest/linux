#include "head.hpp"
#include "output.hpp"

using namespace std;

//使用共享内存

int main()
{
    int fd = openfifo(PATH_FIFO, READER);
    assert(fd > 0);

    // creat key
    key_t key = Creat_key();
    cout << key << endl;

    // get shm
    int shmid = shmget(key, SIZE, IPC_CREAT);
    if (shmid == -1)
    {
        output() << "shmget error :" << strerror(errno) << endl;
        return 1;
    }

    // attch
    char *shm = (char *)shmat(shmid, nullptr, 0);

    // use shm
    //  int index = 0;
    //  while(index != 26)
    //  {
    //      shm[index] = 'A' + index;
    //      ++index;
    //      shm[index] = '\0';
    //      sleep(1);
    //  }
    while (1)
    {
        printf("Please import$$");
        fflush(stdout);
        ssize_t sz = read(0, shm, SIZE);
        if (sz > 0)
        {
            shm[sz] = '\0';
        }

    }

    // detach shm
    shmdt(shm);
}