#pragma once

#include <iostream>
#include <cstring>
#include <stdlib.h>
#include <cerrno>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <unistd.h>
#include <cassert>
#include "output.hpp"

#define PATH "/home/Yewei/Code/study20221112"
#define PROJ_ID 0x12
#define SIZE 4096
#define PATH_FIFO ".fifo"

using namespace std;

key_t Creat_key()
{
    key_t key = ftok(PATH, PROJ_ID);
    if (key == -1)
    {
        std::cerr << "ftok erro:" << strerror(errno) << std::endl;
        exit(1);
    }
    return key;
}

void Createfifo()
{
    umask(0);
    if (mkfifo(PATH_FIFO, 0666) < 0)
    {
        output() << "mkfifo error:" << strerror(errno) <<'\n';
        exit(3);
    }
}

#define READER O_RDONLY
#define WRITER O_WRONLY
int openfifo(const std::string &filename, int flag)
{
    int fd = open(PATH_FIFO,flag);
    if(fd > 0)
    {
        cout<<fd<<endl;
    }
    if(fd == -1)
    {
        cerr << strerror(errno)<<endl;
    }
    return fd;
}

int readfifo(int fd)
{
    uint32_t status = 0;
    ssize_t sz = read(fd, &status, sizeof(status));
    if (sz == -1)
    {
        cout << "read error" << endl;
    }
    return sz;
}

int writefifo(int fd)
{
    uint32_t num = 1;
    ssize_t t = write(fd, &num, sizeof(num));
    if (t == -1)
    {
        cout << "write error" << endl;
    }
}

int closefifo(int fd,const std::string filename)
{
    close(fd);
    unlink(filename.c_str());
}
