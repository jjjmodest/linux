#include "head.hpp"
#include "output.hpp"

using namespace std;

//创建共享内存

const int flag = IPC_CREAT | IPC_EXCL;

int main()
{
    Createfifo();
    int fd = openfifo(PATH_FIFO,O_WRONLY);

    // create key
    key_t key = Creat_key();
    output() << "key: " << key << endl
             << endl;

    // create shm
    int shmid = shmget(key, SIZE, flag | 0666);
    if (shmid == -1)
    {
        output() << "shmget error :" << strerror(errno) << endl;
        return 1;
    }
    output() << "shmget success! shmid:" << shmid << endl
             << endl;

    // attach proc
    int cnt = 3;
    while (cnt--)
    {
        cout << "attach倒计时::" << cnt << endl;
    }
    //今天我们以一个字节一个字节来访问共享内存
    char *shm = (char *)shmat(shmid, nullptr, 0);
    output() << "attach success!" << endl
             << endl;

    // use shm
    while (1)
    {
        if (readfifo(fd) <= 0)
        {
            break;
        }
        printf("%s", shm);
        sleep(1);
    }

    // detach shm
    cnt = 3;
    while (cnt--)
    {
        cout << "detach倒计时::" << cnt << endl;
    }
    shmdt(shm);
    output() << "detach success!" << endl
             << endl;

    // delte shm
    cnt = 3;
    while (cnt--)
    {
        cout << "删除倒计时::" << cnt << endl;
    }
    int shmrm = shmctl(shmid, IPC_RMID, nullptr);
    if (shmrm == -1)
    {
        output() << "shmctl error :" << strerror(errno) << endl;
    }
    output() << "shmctl success!" << endl;

    closefifo(fd, PATH_FIFO);
    return 0;
}