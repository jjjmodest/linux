#include <iostream>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
using namespace std;

void fuc(int sig)
{
    while (waitpid(-1, NULL, 0) > 0)
    {
        cout << "回收子进程成功" << endl;
    }
}

int main()
{
    signal(SIGCHLD, SIG_IGN);

    for (int i = 0; i < 3; i++)
    {
        pid_t pid = fork();
        if (pid == 0)
        {
            // child
            int cnt = 6;
            while (cnt--)
            {
                cout << "我是子进程, 我的pid是: " << getpid() << endl;
                sleep(1);
            }
            exit(0);
        }
    }


    // // parent
    // waitpid(-1,NULL,0);
    // while(1)
    // {
    //     cout<< "我等到子进程了" << endl;
    //     sleep(1);
    // }

    while (1)
    {
        cout << "我正在欢快的做事情" << endl;
        sleep(2);
    }
}

// volatile int a = 0;

// void fuc(int sig)
// {
//     a = 1;
//     cout<<"数值a 改为 1"<<endl;
// }

// int main()
// {
//     signal(2,fuc);

//     while(!a);
//     cout<<"进程退出"<<endl;

//     return 0;
// }

// void fuc()
// {
//     sigset_t mask, prev_mask;

//     sigfillset(&mask);
//     sigprocmask(SIG_BLOCK,&mask,&prev_mask);
//     ...
//     sigprocmask(SIG_SETMASK,&prev_mask,NULL);

// }

// ssize_t sio_puts(char s[])
// {
//     return write(STDOUT_FILENO,sio_strlen(s));
// }
// ssize_t sio_putl(long v)
// {
//     char s[128];

//     sio_ltoa(v,s,10);
//     return sio_puts(s);
// }

// void sio_error(char s[])
// {
//     sio_puts(s);
//     _exit(1);
// }
// void fuc(int sig)
// {
//     cout << "我正在执行自定义处理程序，捕捉到的信号为： " << sig << endl;
//     int cnt = 5;
//     while (cnt--)
//     {
//         for (int signum = 1; signum <= 31; signum++)
//         {
//             sigset_t pending;
//             sigpending(&pending);
//             if (sigismember(&pending, signum))
//             {
//                 cout << '1';
//             }
//             else
//             {
//                 cout << '0';
//             }
//         }
//         cout << endl;
//         sleep(1);
//     }
// }

// int main()
// {
//     signal(2,fuc);
//     while(1)
//     {
//         cout<<"main正在运行"<<endl;
//     }
// }
