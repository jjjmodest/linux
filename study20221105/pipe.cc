#include <iostream>
#include <cstdio>
#include <unistd.h>
#include <cstring>
#include <sys/wait.h>
#include <sys/types.h>
using namespace std;

int main()
{
    // 1.创建管道
    int pipefd[2];
    if (pipe(pipefd) != 0)
    {
        cerr << "pipe failed" << endl;
        return 1;
    }

    // 2.创建子进程
    pid_t pid = fork();
    if (pid < 0)
    {
        //失败就打印
        cerr << "fork failed" << endl;
    }
    else if (pid == 0)
    {
        //子进程
        //读
        close(pipefd[1]);
        const int sz = 128;
        char buf[sz];
        while (1)
        {
            memset(buf, 0, sizeof(buf));
            //从匿名pipe中读取，放入buf中
            ssize_t s = read(pipefd[0], buf, sizeof(buf) - 1);
            if (s > 0)
            {
                //读取成功
                buf[s] = '\0';
                cout << "读取成功，结果是：" << buf << endl;
            }
            else if (s == 0)
            {
                //读取结束
                cout << "他结束，我也" << endl;
                break;
            }
            else
            {
                // 防止报错
            }
        }
        close(pipefd[0]);
        exit(123);
    }
    else
    {
        //父进程
        //写
        close(pipefd[0]);
        string str = "我是父进程，你慈祥的父亲，我的子进程你好!!!";
        int n = 0;
        while (n != 5)
        {
            //往pipefd[1]匿名管道中写
            write(pipefd[1], str.c_str(), str.size());
            sleep(1);
            n++;
        }
        close(pipefd[1]);
    }
    //等待子进程
    int status = 0;
    pid_t wt = waitpid(pid, &status, 0);
    if( WIFEXITED(status))
    {
        cout<<"等待成功"<<"退出码是："<< WEXITSTATUS(status) <<endl;
    }

    return 0;
}