#include <iostream>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <string>
#include <cstring>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "log.hpp"
using namespace std;

static void Usage(const string proc)
{
    cout << "Usage:\n\t" << proc << " port [ip]" << endl;
}

class UdpServer
{
public:
    UdpServer(int port, string ip = "") : _ip(ip), _sockfd(-1), _port((uint16_t)port)
    {
    }
    ~UdpServer()
    {
    }

public:
    void init()
    {
        // 1.create 套接字
        _sockfd = socket(AF_INET, SOCK_DGRAM, 0);
        if (_sockfd < 0)
        {
            logMessage(FATAL, "socket: %s%d", strerror(errno), _sockfd);
            exit(1);
        }
        logMessage(DEBUG, "socket create success : %d", _sockfd);
        // 2 bind
        // 2.1 填入基本信息到struct sockaddr_in 中
        struct sockaddr_in local;
        // 初始化
        bzero(&local, sizeof(local));
        // 填充域 AF_INET 网络通信 AF_UNIX 本地通信
        local.sin_family = AF_INET;
        // 填充对应的端口号 htons的作用是将本地序列转换为网络序列这样才能发送给对方
        local.sin_port = htons(_port);
        // 服务器的IP地址 xx.yy.aa.ccc 每个都是0-255的数字，有四个8比特位 正好放在uint36_t中
        // sin_addr也是一个结构体其中的元素是s._addr是被typedef过的uint32_t
        // INADDR_ANY就是0，一般不关注服务器绑定哪一个IP地址，服务器会自动bind，一般所有服务器都是这样做的
        // inet_addr将char *转换为s_addr，还会将主机序列转换为网络序列
        local.sin_addr.s_addr = _ip.empty() ? INADDR_ANY : inet_addr(_ip.c_str());
        // 2.2 bind网络信息
        if (bind(_sockfd, (const struct sockaddr *)&local, sizeof(local)) == -1)
        {
            logMessage(FATAL, "bind: %s%d", strerror(errno), _sockfd);
            exit(2);
        }
        logMessage(DEBUG, "socket success: %d", _sockfd);
    }
    void start()
    {
        char inbuffer[1024];  // 输入进来的数据放到inbuffer中
        char outbuffer[1024]; // 输出的数据放outbuffer中
        while (1)
        {
            struct sockaddr_in peer;// 输出形参数
            socklen_t len = sizeof(peer);// 输入输出型参数
            recvfrom(_sockfd, inbuffer, sizeof(inbuffer) - 1, 0,(struct sockaddr*)&peer,&len);
        }
    }

private:
    string _ip;
    int _sockfd;
    uint16_t _port;
};
int main(int argc, char *argv[])
{
    if (argc != 2 && argc != 3)
    {
        Usage(argv[0]);
        exit(3);
    }
    uint16_t port = atoi(argv[1]);
    string ip;
    if (argc == 3)
    {
        ip = argv[2];
    }

    UdpServer svr(port, ip);
    svr.init();
    svr.start();

    return 0;
}