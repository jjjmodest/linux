#include <iostream>
#include <pthread.h>
#include <unistd.h>
using namespace std;

void *thread_fuc(void *argc)
{
    char *msg = static_cast<char *>(argc);
    int cnt = 2;
    while (cnt--)
    {
        cout <<"我是"<< msg <<"我的线程ID: "<<pthread_self()<< endl;
        sleep(1);
    }
}

int main()
{
    pthread_t tid;
    pthread_create(&tid, NULL, thread_fuc, (void *)"thread1");
    int cnt = 2;
    while (cnt--)
    {
        cout << "我是主线程" <<"我的线程ID: "<<pthread_self()<< endl;
        sleep(1);
    }

    pthread_join(tid,NULL);
    cout<<"pthread_join success!"<<endl;

    return 0;
}